using NeuralNetworkAPI;
using NeuralNetworkAPI.Data;
using NeuralNetworkAPI.Extensions;
using NeuralNetworkAPI.Logging;
using NeuralNetworkAPI.Models;
using static NeuralNetworkAPI.Extensions.Utils;

Log.LoggingLevel = LoggingLevel.DEBUG;

if (args.Length > 0)
{
    if (args[0].Equals("gs"))
    {
        Utils.GridsearchMode = true;        // Disables some logging
        GridSearch.Search();
        Log.Info($"Gridsearch complete. Exiting...");
        Environment.Exit(0);
    }
}

int dataSetSize = 40000;
double trainSetSize = 2000;
double trainTestRatio = (double)trainSetSize / (double)(dataSetSize + trainSetSize);
if (dataSetSize % 100 != 0) { throw new Exception("The training dataset size has to be dividable by 100"); }

int maxEpochs = 100;
int networkInputImageSize = 28;
int networkInputImagePadding = 2;   // conv: 2

var neuralNetwork = new NeuralNetwork(networkInputImageSize,
    classifierLearningRate: Math.Pow(10, -3),
    convolutionalLearningRate: Math.Pow(10, -9));
neuralNetwork.AddConvolutionalLayer(kernels: 32, kernelSize: 3, ActivationFunction.LeakyReLU, 3);
neuralNetwork.AddClassifierLayer(10, ActivationFunction.LeakyReLU);
neuralNetwork.AddClassifierLayer(10, ActivationFunction.Softmax);

#region API Builder

string allowedOrigins = "_myAllowSpecificOrigins";

// .NET 6 Application builder
var builder = WebApplication.CreateBuilder(args);

// CORS policy
builder.Services.AddCors(options =>
{
    options.AddPolicy(name: allowedOrigins,
        policy =>
        {
            policy.AllowAnyOrigin();
            policy.AllowAnyMethod();
            policy.AllowAnyHeader();
            //policy.WithOrigins(
            //    "http://localhost:4201",    // This
            //    "http://localhost:4200"     // frontend
            //);
        });
});

// Database (RAM, not persistent)
//builder.Services.AddDbContext<NeuralDB>(options => options.Use("Brains"));
// Add services to the container.

var app = builder.Build();

// Configure the HTTP request pipeline.
string url = "http://0.0.0.0:4201";
app.Urls.Add(url);

// Set the CORS policy
app.UseCors(allowedOrigins);

#endregion API Builder

#region Neural Network

Log.Info("Training Neural Network...");
Log.Info("Loading and normalizing/standardizing training data into memory...");
var trainingData = new TrainingData(
    Path.Combine(Environment.CurrentDirectory, "Data", "train.csv"),
    networkInputImageSize, networkInputImagePadding,
    dataSetSize, trainTestRatio);
//Log.Info($"{trainingData.TrainingSet.Count} training samples loaded");
//Log.Info($"{trainingData.TestSet.Count} test samples loaded");

if (OperatingSystem.IsWindows()) { Console.Beep(1000, 1000); }
int e = 0;
List<double> accuracies = new();
//int consecutevilyWorse = 0;
int correct = 0;
double accuracy = 0.0;

double bestAccuracy = 0.0;
int bestAccuracyEpoch = 0;

double CheckAccuracy()
{
    correct = 0;
    foreach (var test in trainingData.TestSet)
    {
        var guess = neuralNetwork.Guess(test.Pixels);
        if (guess == test.Value) { correct++; }
    }
    accuracy = Math.Round(((correct * 1.0) / trainingData.TestSet.Count) * 100.0, 10);

    if (accuracy > bestAccuracy)
    {
        Log.Info($"New best accuracy ({accuracy}%) achieved! Epoch: {e}");
        bestAccuracy = accuracy;
        bestAccuracyEpoch = e;
    }

    return accuracy;
}
List<double> averageLosses = new();
try
{
    while (e < maxEpochs)
    {
        List<double> losses = new();
        e++;
        Log.Info($"Epoch {e}, training...");
        for (int batch = 0; batch < (trainingData.TrainingSet.Count / 100.0); batch++)
        {
            Log.Debug($"Epoch {e}, batch {batch + 1}/{(int)(trainingData.TrainingSet.Count / 100.0)}");
            var subTrainingSet = trainingData.TrainingSet.GetRange(batch * 100, 100);

            foreach (var sample in subTrainingSet)
            {
                neuralNetwork.Train(V.DenseOfArray(sample.Pixels), sample.Value);
                losses.Add(neuralNetwork.GetLastLoss(sample.Value));
            }
        }
        accuracy = CheckAccuracy();
        accuracies.Add(accuracy);
        Log.Debug($"Confidence: {string.Format("{0:00.00}", accuracy)}% after {e} epochs");

        var averageLoss = losses.Average();
        averageLosses.Add(averageLoss);
        Log.Debug($"Average loss: {averageLoss} after {e} epochs");

        if (e > 10)
        {
            double accuracyTrend = 0.0;
            double lossTrend = 0.0;
            for (int i = 10; i >= 1; i--)
            {
                accuracyTrend += accuracies[^i] - accuracies[^(i + 1)];
                lossTrend += averageLosses[^i] - averageLosses[^(i + 1)];
            }
            if (accuracyTrend < 0 || lossTrend > 0)
            {
                Log.Info("Accuracy or Loss trends show decline in network performance over the last 10 epochs. Training halted.");
                break;
            }
        }
        Utils.PlotGraph("Epochs", "Accuracy", accuracies.ToArray(), "Loss", averageLosses.ToArray());
    }
}
catch (Exception exc)
{   // Prevents a crash and tries to produce results
    Log.Error($"An error occured during training. Training is halted. The exception message:\n{exc.Message}");
}

Log.Info("Determining the final accuracy...");
List<Tuple<int, int>> results = new();
foreach (var test in trainingData.TestSet)
{
    var guess = neuralNetwork.Guess(test.Pixels);
    results.Add(Tuple.Create((guess == test.Value ? 1 : 0), test.Value));
}
double finalAccuracy = results.Where(x => x.Item1 == 1).Count() * 1.0 / results.Count;
finalAccuracy = Math.Round(finalAccuracy * 100.0, 2);

Log.Info("Plotting accuracy curve over epochs...");
Utils.PlotGraph("Epochs", "Accuracy", accuracies.ToArray());

Log.Info("Plotting loss curve with respect to accuracy...");
Utils.PlotGraph("Epochs", "Accuracy", accuracies.ToArray(), "Loss", averageLosses.ToArray());

Log.Info("Determining the hardest numbers to recognize...");
var incorrects = results.Where(x => x.Item1 == 0).ToList();

Log.Info("Creating heatmaps of the kernels...");
var kernels = neuralNetwork.GetKernels();

for (int i = 0; i < kernels.Count; i++)
{
    PlotHeatmap(e, "kernel", i, kernels[i].ToArray());
}

int exampleNumber = 4;
Log.Info($"Creating heatmaps of convolution output of the number {exampleNumber}...");
var values = neuralNetwork.GetConvolutionalLayerOutput(trainingData.TrainingSet.Where(x => x.Value == exampleNumber).FirstOrDefault());
for (int i = 0; i < values.Count; i++)
{
    PlotHeatmap(e, $"ConvExample_{exampleNumber}_", i, values[i].ToArray());
}

Log.Info($"The best achieved accuracy was {string.Format("{0:00.00}", bestAccuracy)}% after {bestAccuracyEpoch} epochs");
var errorprone = incorrects.GroupBy(x => x.Item2).OrderByDescending(y => y.Count()).Select(x => x.Key);
Log.Debug("The most difficult numbers in order (most -> least):");
foreach (int i in errorprone) { Log.Debug($"{i}"); }

// Kaggle entry csv
Log.Info("Creating Kaggle competition entry CSV...");
var kaggleCSVFile = new FileInfo(Path.Combine("kaggleEntry.csv"));
var c = kaggleCSVFile.Create();
c.Close();
c.Dispose();

var csvFilePath = kaggleCSVFile.FullName;
TrainingData kaggleTestData = TrainingData.GetKaggleTestData(Path.Combine("Data", "test.csv"), networkInputImageSize, networkInputImagePadding);

int id = 1;
string kaggleEntry = "ImageId,Label\n";
foreach (var sample in kaggleTestData.TestSet)
{
    int guess = neuralNetwork.Guess(sample.Pixels);
    //Log.Debug($"This is a {guess}: {sample.Pixels.ToPixelString()}");
    kaggleEntry += $"{id++},{guess}\n";
}
var writer = kaggleCSVFile.AppendText();
writer.WriteLine(kaggleEntry);
writer.Close();
writer.Dispose();

Log.Info($"Neural API is ready! Confidence/Accuracy: {string.Format("{0:00.00}", finalAccuracy)}%");

if (OperatingSystem.IsWindows()) { Console.Beep(1000, 1000); }

#endregion Neural Network

#region API Endpoints

app.MapPost("/brain/guess", (GrayScaleImage gsi) =>
{
    if (gsi.Data is null) { return Results.BadRequest(); }

    List<double> pixels = new();
    foreach (int grayScaleValue in gsi.Data)
    {
        pixels.Add(grayScaleValue / 255.0);
    }

    var hwn = new HandwrittenNumber(0, pixels.ToArray(), networkInputImageSize, networkInputImagePadding);
    Log.Info($"Request image:\n{hwn.Pixels.ToPixelString()}");

    var guess = neuralNetwork.Guess(hwn.Pixels);
    Log.Info($"Brain guessed {guess}");

    return Results.Ok(guess);
});

// Draft, enables the frontend the ability to teach the network
app.MapPost("/brain/teach", (GrayScaleImage gsi, int number) =>
{
    var hwn = new HandwrittenNumber(0, new double[784], networkInputImageSize, networkInputImagePadding);
    for (int i = 0; i < 784; i++) { hwn.Pixels[i] = (gsi.Data[i] / 255.0); }
    Log.Info($"Request image:\n{hwn.Pixels.ToPixelString()}");
    Log.Info($"Request number: {number}");
});

#endregion API Endpoints

app.Run(); // Start the API