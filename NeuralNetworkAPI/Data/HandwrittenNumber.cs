﻿using MathNet.Numerics.LinearAlgebra;
using NeuralNetworkAPI.Extensions;
using NeuralNetworkAPI.Logging;
using static NeuralNetworkAPI.Extensions.Utils;

namespace NeuralNetworkAPI.Data;

public class HandwrittenNumber
{
    public double[] Pixels { get; set; }
    public int Dimensions { get; set; }
    public int Value { get; set; }  // the number that the pixels represent

    public HandwrittenNumber(int value, double[] pixels, int imageSize, int padding)
    {
        Value = value;

        // Remove empty space around the number
        Matrix<double> pixelMatrix = pixels.Unflatten().RemoveZeroPadding(); // Unflatten converts the pixels to a matrix
        
        // Add some of the space back if requested
        if (padding != 0)
        {
            pixelMatrix = pixelMatrix.WithZeroPadding(padding);
        }
        
        // Convert back to flat array and resize to requested image size
        Pixels = pixelMatrix.Flatten().ResizeImage(imageSize, imageSize);

        try { Dimensions = (int)Math.Sqrt(Pixels.Length); }
        catch { throw new Exception("Width and height of the image must be equal"); }
    }

    public override string ToString()
    {
        var handWrittenNumberAsString = $"Number: {Value}, Dimensions: {Dimensions}x{Dimensions}\n";
        handWrittenNumberAsString += Pixels.ToPixelString();
        return handWrittenNumberAsString;
    }
}