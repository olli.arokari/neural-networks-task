﻿using NeuralNetworkAPI.Extensions;
using NeuralNetworkAPI.Logging;

namespace NeuralNetworkAPI.Data;

public class TrainingData
{
    public List<HandwrittenNumber> TrainingSet { get; set; }
    public List<HandwrittenNumber> TestSet { get; set; }

    private TrainingData()
    {
        TrainingSet = new();
        TestSet = new();
    }

    /// <param name="trainingDataFilePath"> The path of the training data file CSV </param>
    /// <param name="samples"> Optional, sets the training set size. Test samples is calculated from this value</param>
    public TrainingData(string trainingDataFilePath, int targetImageSize, int imagePadding, int samples = 0, double testRatio = 0.2)
    {
        TrainingSet = new List<HandwrittenNumber>();
        TestSet = new List<HandwrittenNumber>();

        FileInfo trainingDataFile = new(trainingDataFilePath);
        string fileContent = File.ReadAllText(trainingDataFile.FullName);
        List<string> rows = fileContent.Split("\n").ToList();
        rows.RemoveAt(0); // first row is labels

        if (testRatio == 1.0) { testRatio = 0.999; }  // Never divide by 0
        int totalRequested = (int)Math.Round(samples / (1.0 - testRatio));

        if (totalRequested > rows.Count || samples == 0) { totalRequested = rows.Count; } // if over max, just use all the data there is. Ratio still applies.
        else if (samples != 0) { rows = rows.Take(totalRequested).ToList(); }

        int trainingSet = (int)Math.Round(rows.Count * (1.0 - testRatio));
        foreach (string row in rows)
        {
            var data = row.Split(",");
            int value = int.Parse(data[0]);
            double[] pixels = new double[data.Length - 1];
            for (int j = 1; j < data.Length; j++)
            {
                pixels[j - 1] = (int.Parse(data[j]) * 1.0) / 255.0; // normalize to values between 0.0 to 1.0
            }
            if (TrainingSet.Count < trainingSet) { TrainingSet.Add(new HandwrittenNumber(value, pixels, targetImageSize, imagePadding)); }
            else { TestSet.Add(new HandwrittenNumber(value, pixels, targetImageSize, imagePadding)); }
        }
        // Don't spam the log message if using gridsearch
        if (!Utils.GridsearchMode) { Log.Info($"Loaded {TrainingSet.Count} samples for training, {TestSet.Count} samples for testing ({testRatio * 100.0}%)"); }
    }

    public static TrainingData GetKaggleTestData(string testFilePath, int imageSize, int padding)
    {
        TrainingData td = new();

        FileInfo trainingDataFile = new(testFilePath);
        string fileContent = File.ReadAllText(trainingDataFile.FullName);
        List<string> rows = fileContent.Split("\n").ToList();
        rows.RemoveAt(0); // first row is labels

        foreach (string row in rows)
        {
            try
            {
                var data = row.Split(",");
                double[] pixels = new double[data.Length];
                for (int j = 0; j < data.Length; j++)
                {
                    pixels[j] = (int.Parse(data[j]) * 1.0) / 255.0; // normalize to values between 0.0 to 1.0
                }
                td.TestSet.Add(new HandwrittenNumber(-1, pixels, imageSize, padding));
            }
            catch (Exception e)
            {
                Log.Error($"Failed to parse row from Kaggle Test CSV: {e.Message}");
            }
        }
        return td;
    }
}