﻿using NeuralNetworkAPI.Data;
using NeuralNetworkAPI.Logging;
using NeuralNetworkAPI.Models;
using System.Collections.Concurrent;
using System.Diagnostics;
using static NeuralNetworkAPI.Extensions.Utils;

namespace NeuralNetworkAPI;

public static class GridSearch
{
    private static readonly bool ENABLE_MULTITHREADING = true;

    public static void Search()
    {
        // Prepare the CSV file for the results
        CSVResultManager.NewFile();
        var csvHeader = "NN Learningrate;CNN Learningrate;";
        for (int i = 0; i < 1; i++)
        {
            csvHeader += $"L{i} Kernels;L{i} Kernel Size;L{i} Max Pool Size;L{i} Activation function";
        }
        csvHeader += "Hidden Layer Neurons;Image size;Image padding;Epochs;Runtime;Accuracy";//Training Accuracy;Test Accuracy";
        CSVResultManager.WriteLineToCSV(csvHeader);

        // Set the threadcount so the program knows when to stop
        ThreadPool.GetAvailableThreads(out int initiallyAvailableThreads, out int cpt);

        //
        // Gridsearch settings
        //
        int trainingDataSampleSize = 5000;
        Log.Info($"Starting gridsearch... {(trainingDataSampleSize == 0 ? "" : $"Only {trainingDataSampleSize} training data samples are used")}");
        var startTime = DateTime.Now;

        // Do a test with each combination of the settings
        List<GridSearchParameters> runs = new();

        // Different image sizes to use
        List<int> imageSizes = new()
        {
            16,20,24,28 // top results with 18 and 20, good results with 12 to 24
        };

        for (int convolutionalLayerLearningRateExp = -7; convolutionalLayerLearningRateExp >= -9; convolutionalLayerLearningRateExp--)
        {
            for (int classifierLearningRateExp = -3; classifierLearningRateExp >= -3; classifierLearningRateExp--)
            {
                for (int k1 = 15; k1 <= 35; k1 += 5)
                {
                    for (int s1 = 3; s1 <= 5; s1 += 2) // 3x3 in top results, 5x5 in bottom results. Otherwise all over the place
                    {
                        for (int m1 = 3; m1 <= 3; m1 += 1) // 3 confirmed best, but not that much impact
                        {
                            for (int af1 = 2; af1 <= 2; af1++)  // 2 = LeakyReLU, confirmed best
                            {
                                for (int hlnc = 10; hlnc <= 10; hlnc += 10)
                                {
                                    foreach (int imageSize in imageSizes)
                                    {
                                        for (int padding = 0; padding <= 4; padding += 2) // 2 in the very top results, otherwise all over the place
                                        {
                                            var actf1 = ActivationFunction.Sigmoid;
                                            if (af1 == 1) { actf1 = ActivationFunction.ReLU; }
                                            else if (af1 == 2) { actf1 = ActivationFunction.LeakyReLU; }

                                            List<LayerSettings> layerSettings = new()
                                            {
                                                new(k1, s1, m1, actf1)
                                            };
                                            runs.Add(new GridSearchParameters(trainingDataSampleSize,
                                                convolutionalLayerLearningRateExp, classifierLearningRateExp,
                                                layerSettings, hlnc,
                                                imageSize, padding
                                                ));
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        //
        // Run the setting combinations
        //
        ProgressManager.TotalThreads = runs.Count;
        foreach (var run in runs)
        {
            if (ENABLE_MULTITHREADING)
            {
                ThreadPool.QueueUserWorkItem(state => Test(run, startTime)); ;
            }
            else { Test(run, startTime); }
        }
        if (ENABLE_MULTITHREADING)
        {
            while (ThreadPool.PendingWorkItemCount > 0)
            {
                Thread.Sleep(1000);
            }
            // All tasks have started, now waiting for last ones to finish...
            while (true)
            {
                ThreadPool.GetAvailableThreads(out int currentlyAvailableThreads, out cpt);
                if (initiallyAvailableThreads == currentlyAvailableThreads)
                {
                    break;
                }
                Thread.Sleep(1000);
            }
        }

        // Create plots of the results, sorted by testing accuracy
        var results = ResultCache.GetResults();
        if (results.Count != 0)
        {
            var orderedResults = results.OrderBy(x => x.TestAccuracy[^1]).ToList();

            // Separate the data
            double[] trainingAccuracy = orderedResults.Select(x => x.TrainingAccuracy[^1]).ToArray();    // Note, final accuracy
            double[] testAccuracy = orderedResults.Select(x => x.TestAccuracy[^1]).ToArray();            // Note, final accuracy

            double[] chln = orderedResults.Select(x => x.HiddenLayerNeuronCount).ToArray();
            double[] epochsCount = orderedResults.Select(x => x.Epochs).ToArray();
            double[] convLR = orderedResults.Select(x => (double)x.ConvolutionalLayerLearningRateExponent).ToArray();
            double[] fcLR = orderedResults.Select(x => (double)x.ClassfierLayerLearningRateExponent).ToArray();
            double[] runtimes = orderedResults.Select(x => (double)x.Runtime).ToArray();
            double[] imgSizes = orderedResults.Select(x => (double)x.ImageSize).ToArray();
            double[] imgPaddings = orderedResults.Select(x => (double)x.ImagePadding).ToArray();

            List<List<double>> kernelsCount = new();
            List<List<double>> kernelSizes = new();
            List<List<double>> maxpoolSizes = new();
            List<List<int>> activationFunctions = new();
            for (int i = 0; i < orderedResults[0].ConvolutionalLayerSettings.Count; i++)
            {
                kernelsCount.Add(new());
                kernelSizes.Add(new());
                maxpoolSizes.Add(new());
                activationFunctions.Add(new());
                foreach (var run in orderedResults.Select(x => x.ConvolutionalLayerSettings[i]).ToList())
                {
                    kernelsCount[i].Add(run.Kernels);
                    kernelSizes[i].Add(run.KernelSize);
                    maxpoolSizes[i].Add(run.MaxpoolSize);
                    activationFunctions[i].Add((int)run.ActivationFunction);
                }
            }

            PlotGraph("", "Classifier hidden layer neurons", chln, "Accuracy", testAccuracy);
            for (int i = 0; i < kernelsCount.Count; i++)
            {
                PlotGraph("", $"L{i} Kernels", kernelsCount[i].ToArray(), "Accuracy", testAccuracy);
                PlotGraph("", $"L{i} Kernel size", kernelSizes[i].ToArray(), "Accuracy", testAccuracy);
                PlotGraph("", $"L{i} Maxpool size", maxpoolSizes[i].ToArray(), "Accuracy", testAccuracy);
                List<double> afconverted = new();
                foreach (int af in activationFunctions[i]) { afconverted.Add(af); }
                PlotGraph("", $"L{i} AF 0=Sig, 1=ReLU, 2=LReLU", afconverted.ToArray(), "Accuracy", testAccuracy);
            }
            PlotGraph("", "Image size", imgSizes, "Accuracy", testAccuracy);
            PlotGraph("", "Image padding", imgPaddings, "Accuracy", testAccuracy);
            PlotGraph("", "Epochs", epochsCount, "Accuracy", testAccuracy);
            PlotGraph("", "Convolutional layer learning rate", convLR, "Accuracy", testAccuracy);
            PlotGraph("", "Classifier learning rate", fcLR, "Accuracy", testAccuracy);
            PlotGraph("", "Runtime", runtimes, "Accuracy", testAccuracy);
            foreach (Result r in results)
            {
                double[] trainingAccuracies = r.TrainingAccuracy.ToArray();
                double[] testAccuracies = r.TestAccuracy.ToArray();
                double[] averageLosses = r.AverageLoss.ToArray();
                if (trainingAccuracies.Length < 1 || testAccuracies.Length < 1 || averageLosses.Length < 1) { continue; }

                int convolutionLearningRate = r.ConvolutionalLayerLearningRateExponent;
                int classifierLearningRate = r.ClassfierLayerLearningRateExponent;
                PlotGraph("Epochs", "Training Accuracies", trainingAccuracies, "Average Loss", averageLosses, $"convLR{convolutionLearningRate}_claLR{classifierLearningRate}");
                PlotGraph("Epochs", "Test Accuracies", testAccuracies, "Average Loss", averageLosses, $"convLR{convolutionLearningRate}_claLR{classifierLearningRate}");
            }
        }
        if (OperatingSystem.IsWindows()) { Console.Beep(1000, 800); }
    }

    private static void Test(GridSearchParameters gsp, DateTime starttime)
    {
        #region Settings

        Stopwatch stopwatch = new();

        int maxEpochs = 100;
        var convolutionalLayerLearningRate = Math.Pow(10, gsp.ConvolutionalLayerLearningRateExponent);
        var classifierLearningRate = Math.Pow(10, gsp.ClassifierLearningRateExponent);
        var hiddenLayerNeuronCount = gsp.HiddenLayerNeuronCount;
        var imageSize = gsp.ImageSize;
        var padding = gsp.Padding;
        var trainingData = new TrainingData(
            Path.Combine(Environment.CurrentDirectory, "Data", "train.csv"),
            gsp.ImageSize, gsp.Padding,
            gsp.TrainingDataSampleSize, 2000.0 / gsp.TrainingDataSampleSize);

        NeuralNetwork nn = new(gsp.ImageSize, classifierLearningRate, convolutionalLayerLearningRate);
        try
        {
            foreach (var cLayer in gsp.Layers)
            {
                if (cLayer.Kernels == 0) { continue; }
                nn.AddConvolutionalLayer(cLayer.Kernels, cLayer.KernelSize, cLayer.ActivationFunction, cLayer.MaxpoolSize);
            }
            if (hiddenLayerNeuronCount > 0)
            {
                nn.AddClassifierLayer(hiddenLayerNeuronCount, ActivationFunction.LeakyReLU);
            }
            nn.AddClassifierLayer(10, ActivationFunction.Softmax);
        }
        catch (Exception exc)
        {
            Log.Error($"The neural network could not be created: {exc.Message}");
            ProgressManager.IncrementCompleted();
            return;
        }

        #endregion Settings

        #region Training

        //bool stopOnTrainRate = false;   // Allows stopping the training if the accuracy over the training data is not increased
        int e = 0;
        stopwatch.Start();
        List<double> averageLosses = new();
        List<double> trainingAccuracies = new();
        List<double> testAccuracies = new();
        bool negativeAccuracyTrend = false;
        bool positiveLossTrend = false;
        while (e <= maxEpochs)
        {
            e++;

            // Train
            List<double> losses = new();
            for (int i = 0; i < trainingData.TrainingSet.Count; i++)
            {
                try
                {
                    var sample = trainingData.TrainingSet[i];
                    nn.Train(
                        V.DenseOfArray(sample.Pixels),
                        sample.Value);
                    losses.Add(nn.GetLastLoss(sample.Value));
                }
                catch (Exception exc)
                {
                    Log.Error($"Exception while training: {exc.Message}");
                    testAccuracies.Add(-100.0);
                    trainingAccuracies.Add(-100.0);
                    goto result;
                }
            }

            // Measure average loss of the epoch
            averageLosses.Add(losses.Average());

            // Measure the accuracy of the epoch over the test data
            var correct = 0;
            foreach (var test in trainingData.TestSet)
            {
                try { if (nn.Guess(V.DenseOfArray(test.Pixels)) == test.Value) { correct++; } }
                catch (Exception exc)
                {
                    Log.Error($"Exception while guessing number: {exc.Message}");
                    testAccuracies.Add(-100.0);
                    trainingAccuracies.Add(-100.0);
                    goto result;
                }
            }
            var testAccuracy = Math.Round(((correct * 1.0) / trainingData.TestSet.Count) * 100, 2);

            // Measure the accuracy of the epoch over the training data
            correct = 0;
            foreach (var test in trainingData.TrainingSet)
            {
                try { if (nn.Guess(V.DenseOfArray(test.Pixels)) == test.Value) { correct++; } }
                catch (Exception exc)
                {
                    Log.Error($"Exception while guessing number: {exc.Message}");
                    testAccuracies.Add(-100.0);
                    trainingAccuracies.Add(-100.0);
                    goto result;
                }
            }
            var trainingAccuracy = Math.Round(((correct * 1.0) / trainingData.TrainingSet.Count) * 100, 2);

            // Save the accuracies
            testAccuracies.Add(testAccuracy);
            trainingAccuracies.Add(trainingAccuracy);

            // Stop if accuracy doesn't increase and loss isn't reduced over the last five epochs
            if (e > 10)
            {
                double accuracyTrend = 0.0;
                double lossTrend = 0.0;
                for (int i = 10; i >= 1; i--)
                {
                    accuracyTrend += testAccuracies[^(i)] - testAccuracies[^(i + 1)];
                    lossTrend += averageLosses[^(i)] - averageLosses[^(i + 1)];
                }
                if (accuracyTrend <= 0) { negativeAccuracyTrend = true; }
                if (lossTrend >= 0) { positiveLossTrend = true; }
                if (negativeAccuracyTrend || positiveLossTrend) { break; }   // either trends have at some point reached stop condition
            }
        }

    #endregion Training

    #region Results

    result:
        stopwatch.Stop();
        Result result = new(
            gsp.ConvolutionalLayerLearningRateExponent,
            gsp.ClassifierLearningRateExponent,
            gsp.Layers, hiddenLayerNeuronCount,
            imageSize, padding,
            e - 1,
            stopwatch.ElapsedMilliseconds,
            trainingAccuracies,
            testAccuracies,
            averageLosses);
        //testrates.GetValueOrDefault(e)) ;

        var resultCSV = $"" +
            $"{result.ClassfierLayerLearningRateExponent};" +
            $"{result.ConvolutionalLayerLearningRateExponent};";
        foreach (var layer in gsp.Layers)
        {
            resultCSV +=
                $"{layer.Kernels};" +
                $"{layer.KernelSize};" +
                $"{layer.MaxpoolSize};" +
                $"{layer.ActivationFunction};";
        }
        resultCSV +=
        $"{result.HiddenLayerNeuronCount};" +
        $"{result.ImageSize};" +
        $"{result.ImagePadding};" +
        $"{result.Epochs};" +
        $"{result.Runtime};" +
        $"{result.TestAccuracy}";

        var accuracy = testAccuracies[^1];
        var accuracyString = string.Format("{0:00.00}", accuracy) + "%";
        string message = $"Accuracy: {accuracyString}\tEpochs: {e - 1}";

        #region ETA

        ProgressManager.IncrementCompleted();
        var remaining = ProgressManager.Remaining;
        var progress = ProgressManager.Progress;
        message += $"\tRemaining: {remaining}";
        try
        {
            var secondsSinceStart = DateTime.Now.Subtract(starttime).TotalSeconds;
            var estimatedTotalTime = (1.0 / progress) * secondsSinceStart;
            var remainingSeconds = estimatedTotalTime - secondsSinceStart;
            var eta = TimeSpan.FromSeconds(86400);  // Default 24h
            try { eta = TimeSpan.FromSeconds(remainingSeconds); } catch { }  // In case of overflow
            var etaString = string.Format("{0:hh\\:mm\\:ss}", eta);
            message += $"\tETA: {etaString}";
            var progressString = string.Format("{0:00}", progress * 100.0);
            message += $"\tProgress: {progressString}%";
        }
        catch (Exception exc) { Log.Error(exc.Message); }

        #endregion ETA

        if (accuracy < 50.0)
        {
            Log.Error(message);
        }
        else if (accuracy < 75.0)
        {
            Log.Info(message);
        }
        else
        {
            Log.Debug(message);
        }

        CSVResultManager.WriteLineToCSV(resultCSV);
        ResultCache.AddResult(result);

        #endregion Results
    }

    private class GridSearchParameters
    {
        public int TrainingDataSampleSize { get; set; }
        public int ConvolutionalLayerLearningRateExponent { get; set; }         // Exponent in 10^x
        public int ClassifierLearningRateExponent { get; set; }  // Exponent in 10^x
        public List<LayerSettings> Layers { get; set; }
        public int HiddenLayerNeuronCount { get; set; }
        public bool UseConvolutionalLayers { get; set; }
        public int ImageSize { get; set; }
        public int Padding { get; set; }

        public GridSearchParameters(int trainingDataSampleSize, int cnnLearningRateExp, int fcLearningRateExp, List<LayerSettings> layers, int classifierHiddenLayerNeuronCount, int imageSize, int padding)
        {
            TrainingDataSampleSize = trainingDataSampleSize;
            ConvolutionalLayerLearningRateExponent = cnnLearningRateExp;
            ClassifierLearningRateExponent = fcLearningRateExp;
            Layers = layers;
            HiddenLayerNeuronCount = classifierHiddenLayerNeuronCount;
            ImageSize = imageSize;
            Padding = padding;
        }
    }
}

public class Result
{
    public int ConvolutionalLayerLearningRateExponent { get; set; }
    public int ClassfierLayerLearningRateExponent { get; set; }
    public List<LayerSettings> ConvolutionalLayerSettings { get; set; }
    public double HiddenLayerNeuronCount { get; set; }
    public int ImageSize { get; set; }
    public int ImagePadding { get; set; }
    public double Epochs { get; set; }
    public long Runtime { get; set; }
    public List<double> TrainingAccuracy { get; set; }  // Confidence over the training data after each epoch
    public List<double> TestAccuracy { get; set; }  // Confidence over the test data after each epoch
    public List<double> AverageLoss { get; set; }   // Average loss after each epoch

    public Result(int conLR, int claLR, List<LayerSettings> layerSettings, double hlnc, int imageSize, int imagePadding, double epochs, long runtime, List<double> trainingAccuracies, List<double> testAccuracies, List<double> averageLosses)
    {
        ConvolutionalLayerLearningRateExponent = conLR;
        ClassfierLayerLearningRateExponent = claLR;
        ConvolutionalLayerSettings = layerSettings;
        HiddenLayerNeuronCount = hlnc;
        ImageSize = imageSize;
        ImagePadding = imagePadding;
        Epochs = epochs;
        Runtime = runtime;
        TrainingAccuracy = trainingAccuracies;
        TestAccuracy = testAccuracies;
        AverageLoss = averageLosses;
    }
}

public class LayerSettings
{
    public int Kernels { get; set; }
    public int KernelSize { get; set; }
    public int MaxpoolSize { get; set; }
    public ActivationFunction ActivationFunction { get; set; }

    public LayerSettings(int k, int s, int m, ActivationFunction activationFunction)
    { Kernels = k; KernelSize = s; MaxpoolSize = m; ActivationFunction = activationFunction; }
}

public static class ResultCache
{
    private static readonly ConcurrentBag<Result> results = new();

    public static void AddResult(Result result)
    {
        results.Add(result);
    }

    public static List<Result> GetResults()
    {
        return results.ToList();
    }
}

public static class CSVResultManager
{
    private static readonly string resultDirPath = "Results";
    private static string csvFilePath = "";
    private static readonly ReaderWriterLock Lock = new();

    public static void NewFile()

    {
        // Lock during the creation of the file
        Lock.AcquireWriterLock(int.MaxValue);

        // Directory
        DirectoryInfo resultDir = new(resultDirPath);
        if (!resultDir.Exists) { resultDir.Create(); }

        // File
        int i = 0;
        csvFilePath = Path.Combine(resultDirPath, "gridsearch" + i + ".csv");
        FileInfo csvFile = new(csvFilePath);

        while (csvFile.Exists)
        {
            csvFile = new FileInfo(Path.Combine(resultDirPath, "gridsearch" + i + ".csv"));
            i++;
        }
        var c = csvFile.Create();
        c.Close();
        c.Dispose();
        csvFilePath = csvFile.FullName;
        Lock.ReleaseWriterLock();
    }

    public static void WriteLineToCSV(string line)
    {
        if (csvFilePath == "") { throw new Exception("Run NewFile() first!"); }
        var csvFile = new FileInfo(csvFilePath);
        try
        {
            Lock.AcquireWriterLock(int.MaxValue);
            var writer = csvFile.AppendText();
            writer.WriteLine(line);
            writer.Close();
            writer.Dispose();
        }
        catch (Exception e)
        {
            Log.Error("Failed to write to CSV: " + line + "\nError: " + e);
        }
        finally
        {
            Lock.ReleaseWriterLock();
        }
    }
}

public static class ProgressManager
{
    private static int CompletedThreads = 0;
    public static int TotalThreads { get; set; }
    public static int Remaining { get => TotalThreads - CompletedThreads; }
    public static double Progress { get => (double)CompletedThreads / TotalThreads; }

    public static int GetCompleted()
    { return CompletedThreads; }

    public static void IncrementCompleted()
    { Interlocked.Increment(ref CompletedThreads); }
}