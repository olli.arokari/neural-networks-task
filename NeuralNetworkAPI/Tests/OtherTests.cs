﻿using MathNet.Numerics.LinearAlgebra;
using MathNet.Numerics.LinearAlgebra.Double;
using NeuralNetworkAPI.Extensions;
using Xunit;
using Xunit.Abstractions;
using static NeuralNetworkAPI.Extensions.Utils;

namespace NeuralNetworkAPI.Tests
{
    public class OtherTests
    {
        private readonly ITestOutputHelper logger;

        public OtherTests(ITestOutputHelper output)
        {
            this.logger = output;
        }

        [Fact]
        public void MatrixSubtractMatrix()
        {
            var m1 = DenseMatrix.Build.DenseOfRowArrays(
                new double[] { 1, 2 },
                new double[] { 3, 4 }
                );
            var m2 = DenseMatrix.Build.DenseOfRowArrays(
                new double[] { 0.5, 1.5 },
                new double[] { 7.5, 4 }
                );
            var actual = m1.Subtract(m2);
            Assert.Equal(0.5, actual[0, 0], 10);
            Assert.Equal(0.5, actual[0, 1], 10);
            Assert.Equal(-4.5, actual[1, 0], 10);
            Assert.Equal(0.0, actual[1, 1], 10);
            actual = m1 - m2;
            Assert.Equal(0.5, actual[0, 0], 10);
            Assert.Equal(0.5, actual[0, 1], 10);
            Assert.Equal(-4.5, actual[1, 0], 10);
            Assert.Equal(0.0, actual[1, 1], 10);
        }

        [Fact]
        public void VectorSubtraction()
        {
            var v1 = DenseVector.Build.DenseOfArray(new double[] { 1.0, 2.0, 3.0 });
            var v2 = DenseVector.Build.DenseOfArray(new double[] { 0.5, 1.0, 2.5 });
            var expected = DenseVector.Build.DenseOfArray(new double[] { 0.5, 1.0, 0.5 });
            var actual = v1.Subtract(v2);
            for (int i = 0; i < 3; i++)
            {
                Assert.Equal(expected[i], actual[i]);
            }
        }

        [Fact]
        public void VectorScalarMultiplication()
        {
            var v = Vector<double>.Build.DenseOfArray(new double[] { 0.5, 1.5 });
            var l = 5.0;
            var result = l * v;
            Assert.Equal(5.0 / 2.0, result[0]);
            Assert.Equal(5.0 + 2.5, result[1]);
        }

        [Fact]
        public void VectorMatrixMultiplication()
        {
            var v1 = Vector<double>.Build.DenseOfArray(new double[] { Utils.GetRandomDouble(0, 1), Utils.GetRandomDouble(0, 1) });
            var vm1 = v1.ToColumnMatrix();

            var v2 = Vector<double>.Build.DenseOfArray(new double[] { Utils.GetRandomDouble(0, 1), Utils.GetRandomDouble(0, 1) });
            var vm2 = v2.ToRowMatrix();

            var m = Matrix<double>.Build.DenseOfRowArrays(
                new double[] { Utils.GetRandomDouble(0, 1), Utils.GetRandomDouble(0, 1) },
                new double[] { Utils.GetRandomDouble(0, 1), Utils.GetRandomDouble(0, 1) }
                );

            var r1 = vm1 * vm2;
            Assert.Equal(vm1[0, 0] * vm2[0, 0], r1[0, 0]);
            Assert.Equal(vm1[0, 0] * vm2[0, 1], r1[0, 1]);
            Assert.Equal(vm1[1, 0] * vm2[0, 0], r1[1, 0]);
            Assert.Equal(vm1[1, 0] * vm2[0, 1], r1[1, 1]);

            var r2 = vm1.Multiply(vm2);
            Assert.Equal(vm1[0, 0] * vm2[0, 0], r2[0, 0]);
            Assert.Equal(vm1[0, 0] * vm2[0, 1], r2[0, 1]);
            Assert.Equal(vm1[1, 0] * vm2[0, 0], r2[1, 0]);
            Assert.Equal(vm1[1, 0] * vm2[0, 1], r2[1, 1]);

            Assert.Throws<ArgumentException>(() => vm1 * m);
            Assert.Throws<ArgumentException>(() => vm1.Multiply(m));
        }

        [Fact]
        public void MatrixCap()
        {
            var m = Utils.M.DenseOfArray(new double[,]
            {
                { -4, -2 },
                { 3, 4 },
            });
            m.Cap(3);
            Assert.Equal(-3, m[0, 0]);
            Assert.Equal(-2, m[0, 1]);
            Assert.Equal(3, m[1, 0]);
            Assert.Equal(3, m[1, 1]);
        }

        [Fact]
        public void RemoveZeroPaddingTest()
        {
            Matrix<double> m = M.DenseOfArray(new double[,]
            {
                { 0,0,0 },
                { 0,1,0 },
                { 0,0,0 }
            });

            Matrix<double> mwozp = m.RemoveZeroPadding();
            logger.WriteLine("Before:");
            logger.WriteLine(m.ToString());
            logger.WriteLine("Without zero padding:");
            logger.WriteLine(mwozp.ToString());
            Assert.Equal(1, mwozp.RowCount);
            Assert.Equal(1, mwozp.ColumnCount);
            Assert.Equal(1, mwozp[0, 0]);

            m = M.DenseOfArray(new double[,]
            {
                { 0,0,0,0 },
                { 0,1,0,0 },
                { 0,1,0,0 },
                { 0,0,0,0 }
            });
            mwozp = m.RemoveZeroPadding();
            logger.WriteLine("Before:");
            logger.WriteLine(m.ToString());
            logger.WriteLine("Without zero padding:");
            logger.WriteLine(mwozp.ToString());
            Assert.Equal(2, mwozp.RowCount);
            Assert.Equal(2, mwozp.ColumnCount); // preserve square shape
            Assert.Equal(0, mwozp[0, 0]);
            Assert.Equal(0, mwozp[1, 0]);
            Assert.Equal(1, mwozp[0, 1]);
            Assert.Equal(1, mwozp[1, 1]);
        }
    }
}