﻿using NeuralNetworkAPI.Models;
using Xunit;

using static NeuralNetworkAPI.Extensions.Utils;

namespace Tests;

public class ClassifierTests
{
    [Fact]
    public void ForwardPropagate()
    {
        NeuralNetwork nn = new(28);
        nn.AddClassifierLayer(2, ActivationFunction.Softmax);
        var input = V.Dense(784, 0.5);
        nn.Guess(input);
    }

    [Fact]
    public void Backpropagate()
    {
        NeuralNetwork nn = new(28);
        nn.AddClassifierLayer(784, ActivationFunction.LeakyReLU);
        nn.AddClassifierLayer(10, ActivationFunction.Softmax);
        var input = V.Dense(784);
        for (int i = 0; i < input.Count; i++)
        {
            input[i] = new Random().NextDouble();
        }
        nn.Train(input, 1);
    }
}