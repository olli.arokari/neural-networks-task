﻿using MathNet.Numerics.LinearAlgebra;
using MathNet.Numerics.LinearAlgebra.Double;
using NeuralNetworkAPI.Extensions;
using Xunit;
using Xunit.Abstractions;

namespace Tests;

public class ExtensionsTests
{
    private readonly ITestOutputHelper logger;
    private static Dictionary<int, Matrix<double>>? TestMatrices { get; set; }

    public ExtensionsTests(ITestOutputHelper output)
    {
        this.logger = output;
        TestMatrices = new();
        for (int d = 2; d <= 4; d++)
        {
            var m = Matrix<double>.Build.Dense(d, d);
            for (int i = 0; i < Math.Pow(d, 2); i++)
            {
                var row = (int)Math.Floor((i * 1.0) / d);
                var col = i % d;
                m[row, col] = i + 1;
            }
            TestMatrices.Add(d, m);
        }
    }

    [Fact]
    public void MatrixCopy()
    {
        var m = Matrix<double>.Build.Dense(2, 2, 0); // 2x2 matrix where all values are 0
        var m2 = DenseMatrix.OfMatrix(m);
        m[0, 0] = 1;
        Assert.NotEqual(m[0, 0], m2[0, 0]);
    }

    [Fact]
    public void VectoryCopy()
    {
        var v = Vector<double>.Build.DenseOfArray(new double[] { 0, 1, 2 }); // 2x2 matrix where all values are 0
        var v2 = DenseVector.OfVector(v);
        v[0] = 1;
        Assert.NotEqual(v[0], v2[0]);
    }

    [Fact]
    public void MatrixToArray()
    {
        foreach (var tm in TestMatrices)
        {
            var d = tm.Key;
            var m = tm.Value;
            var a = m.Flatten();
            for (int i = 0; i < Math.Pow(d, 2); i++)
            {
                Assert.Equal(i + 1, a[i], 1);
            }
        }
    }

    [Fact]
    public void ArrayToMatrix()
    {
        foreach (var tm in TestMatrices)
        {
            var m = tm.Value;
            var a = m.Flatten();
            var M = a.Unflatten();
            for (int row = 0; row < M.RowCount; row++)
            {
                for (int col = 0; col < M.ColumnCount; col++)
                {
                    Assert.Equal((row * M.ColumnCount) + col + 1, M[row, col], 1);
                }
            }
        }
    }

    [Fact]
    public void MatrixToArrayToMatrixToArray()
    {   // Check for some weird rotation shenanigans not necessarily cought in the other tests
        foreach (var tm in TestMatrices)
        {
            var d = tm.Key;
            var m = tm.Value;
            var a = m.Flatten();
            for (int i = 0; i < Math.Pow(d, 2); i++)
            {
                Assert.Equal(i + 1, a[i], 1);
            }
            var M = a.Unflatten();
            for (int row = 0; row < d; row++)
            {
                for (int col = 0; col < d; col++)
                {
                    Assert.Equal((row * d) + col + 1, M[row, col], 1);
                }
            }
            a = m.Flatten();
            for (int i = 0; i < Math.Pow(d, 2); i++)
            {
                Assert.Equal(i + 1, a[i], 1);
            }
        }
    }
}