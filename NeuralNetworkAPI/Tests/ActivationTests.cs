﻿using MathNet.Numerics.LinearAlgebra;
using MathNet.Numerics.LinearAlgebra.Double;
using NeuralNetworkAPI.Extensions;
using NeuralNetworkAPI.Models;
using Xunit;
using static NeuralNetworkAPI.Extensions.ActivationFunctions;

namespace NeuralNetworkAPI.Tests
{
    public class ActivationFunctionsTests
    {
        [Fact]
        public void Sigmoid()
        {
            var v = Vector<double>.Build.Dense(2);
            v[0] = 0.1;
            v[1] = 0.3;
            var t = DenseVector.OfVector(v);

            Assert.Equal(0.1, v[0]);
            Assert.Equal(0.3, v[1]);
            v = v.Activate(ActivationFunction.Sigmoid);
            Assert.Equal(0.52, Math.Round(v[0], 2));
            Assert.Equal(0.57, Math.Round(v[1], 2));
        }

        [Fact]
        public void ReLU()
        {
            var testData = new double[] {
            -2.5,
            -1.0,
            -0.1,
            0.0,
            0.1,
            1.0,
            2.5
        };
            var testVector = Vector<double>.Build.DenseOfArray(testData);
            var result = testVector.Activate(ActivationFunction.ReLU);
            Assert.Equal(0.0, result[0], 2);
            Assert.Equal(0.0, result[1], 2);
            Assert.Equal(0.0, result[2], 2);
            Assert.Equal(0.0, result[3], 2);
            Assert.Equal(0.1, result[4], 2);
            Assert.Equal(1.0, result[5], 2);
            Assert.Equal(2.5, result[6], 2);
        }

        [Fact]
        public void ReLU_Derivative()
        {
            var testData = new double[] {
            -2.5,
            -1.0,
            -0.1,
            0.0,
            0.1,
            1.0,
            2.5
        };
            var testVector = Vector<double>.Build.DenseOfArray(testData);
            var result = testVector.DerivativeOf(ActivationFunction.ReLU);
            Assert.Equal(0.0, result[0], 2);
            Assert.Equal(0.0, result[1], 2);
            Assert.Equal(0.0, result[2], 2);
            Assert.Equal(0.0, result[3], 2);
            Assert.Equal(1.0, result[4], 2);
            Assert.Equal(1.0, result[5], 2);
            Assert.Equal(1.0, result[6], 2);
        }

        [Fact]
        public void LeakyReLU()
        {
            var testData = new double[] {
            -2.5,
            -1.0,
            -0.1,
            0.0,
            0.1,
            1.0,
            2.5        };
            var testVector = Vector<double>.Build.DenseOfArray(testData);
            var result = testVector.Activate(ActivationFunction.LeakyReLU);
            Assert.Equal(-0.25, result[0], 2);
            Assert.Equal(-0.1, result[1], 2);
            Assert.Equal(-0.01, result[2], 2);
            Assert.Equal(0.0, result[3], 2);
            Assert.Equal(0.1, result[4], 2);
            Assert.Equal(1.0, result[5], 2);
            Assert.Equal(2.5, result[6], 2);
        }

        [Fact]
        public void LeakyReLU_Derivative()
        {
            var testData = new double[] {
            -2.5,
            -1.0,
            -0.1,
            0.0,
            0.1,
            1.0,
            2.5
        };
            var testVector = Vector<double>.Build.DenseOfArray(testData);
            var result = testVector.DerivativeOf(ActivationFunction.LeakyReLU);
            Assert.Equal(0.1, result[0], 2);
            Assert.Equal(0.1, result[1], 2);
            Assert.Equal(0.1, result[2], 2);
            Assert.Equal(0.1, result[3], 2);
            Assert.Equal(1.0, result[4], 2);
            Assert.Equal(1.0, result[5], 2);
            Assert.Equal(1.0, result[6], 2);
        }

        [Fact]
        public void Softmax()
        {
            Assert.Equal(2.7182818, Math.Exp(1), 2);

            var testData = new double[] {
            -2.5,
            -1.0,
            -0.1,
            0.0,
            0.1,
            1.0,
            2.5 };
            var testVector = Vector<double>.Build.DenseOfArray(testData);
            var result = testVector.Activate(ActivationFunction.Softmax);
            double expSum = 0.0;
            foreach (var td in testData)
            {
                expSum += Math.Exp(td);
            }
            for (int i = 0; i < testData.Length; i++)
            {
                Assert.True(result[i] > 0);
                Assert.True(result[i] < 1);
                Assert.Equal((Math.Exp(testData[0]) / expSum), result[0], 2);
            }
        }
    }
}