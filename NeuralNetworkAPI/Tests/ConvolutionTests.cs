﻿using MathNet.Numerics.LinearAlgebra;
using NeuralNetworkAPI.Extensions;
using Xunit;
using Xunit.Abstractions;

using static NeuralNetworkAPI.Extensions.ConvolutionExtensions;

namespace Tests;

public class ConvolutionalNeuralNetworkTests
{
    #region properties & constructor

    private readonly ITestOutputHelper logger;
    private static Dictionary<int, Matrix<double>>? TestMatrices { get; set; }

    public ConvolutionalNeuralNetworkTests(ITestOutputHelper output)
    {
        this.logger = output;
        TestMatrices = new();
        for (int d = 2; d <= 5; d++)
        {
            var m = Matrix<double>.Build.Dense(d, d);
            for (int i = 0; i < Math.Pow(d, 2); i++)
            {
                var row = (int)Math.Floor((i * 1.0) / d);
                var col = i % d;
                m[row, col] = i + 1;
            }
            TestMatrices.Add(d, m);
        }
    }

    #endregion properties & constructor

    [Fact]
    public void Rotate180Test()
    {
        var m = TestMatrices.GetValueOrDefault(3);
        Assert.Equal(1, m[0, 0], 1);
        Assert.Equal(2, m[0, 1], 1);
        Assert.Equal(3, m[0, 2], 1);
        Assert.Equal(4, m[1, 0], 1);
        Assert.Equal(5, m[1, 1], 1);
        Assert.Equal(6, m[1, 2], 1);
        Assert.Equal(7, m[2, 0], 1);
        Assert.Equal(8, m[2, 1], 1);
        Assert.Equal(9, m[2, 2], 1);
        var rm = m.Rotate180();
        logger.WriteLine($"Rotated (180) matrix:\n{rm}");
        Assert.Equal(9, rm[0, 0], 1);
        Assert.Equal(8, rm[0, 1], 1);
        Assert.Equal(7, rm[0, 2], 1);
        Assert.Equal(6, rm[1, 0], 1);
        Assert.Equal(5, rm[1, 1], 1);
        Assert.Equal(4, rm[1, 2], 1);
        Assert.Equal(3, rm[2, 0], 1);
        Assert.Equal(2, rm[2, 1], 1);
        Assert.Equal(1, rm[2, 2], 1);
    }

    [Fact]
    public void SeparateTest()
    {
        var values = Vector<double>.Build.DenseOfArray(new double[] { 1, 2, 3, 4 });
        var kernelCount = 2;
        var result = Unpool(values, kernelCount);
        Assert.Equal(1, result[0][0], 1);
        Assert.Equal(2, result[0][1], 1);
        Assert.Equal(3, result[1][0], 1);
        Assert.Equal(4, result[1][1], 1);

        values = Vector<double>.Build.DenseOfArray(new double[] { 1, 2, 3, 4, 5, 6 });
        kernelCount = 3;
        result = Unpool(values, kernelCount);
        Assert.Equal(1, result[0][0], 1);
        Assert.Equal(2, result[0][1], 1);
        Assert.Equal(3, result[1][0], 1);
        Assert.Equal(4, result[1][1], 1);
        Assert.Equal(5, result[2][0], 1);
        Assert.Equal(6, result[2][1], 1);
    }

    [Fact]
    public void ZeroPaddingTest()
    {
        var m = TestMatrices.GetValueOrDefault(2);
        logger.WriteLine($"Matrix before zero padding:\n{m}");
        m = m.WithZeroPadding(1);
        logger.WriteLine($"Matrix after zero padding:\n{m}");

        Assert.Equal(0, m[0, 0], 1);
        Assert.Equal(0, m[0, 1], 1);
        Assert.Equal(0, m[0, 2], 1);
        Assert.Equal(0, m[0, 3], 1);

        Assert.Equal(0, m[1, 0], 1);
        Assert.Equal(1, m[1, 1], 1);
        Assert.Equal(2, m[1, 2], 1);
        Assert.Equal(0, m[1, 3], 1);

        Assert.Equal(0, m[2, 0], 1);
        Assert.Equal(3, m[2, 1], 1);
        Assert.Equal(4, m[2, 2], 1);
        Assert.Equal(0, m[2, 3], 1);

        Assert.Equal(0, m[3, 0], 1);
        Assert.Equal(0, m[3, 1], 1);
        Assert.Equal(0, m[3, 2], 1);
        Assert.Equal(0, m[3, 3], 1);

        m = TestMatrices.GetValueOrDefault(3);
        m = m.WithZeroPadding(2);
        Assert.Equal(0, m[0, 0], 1);
        Assert.Equal(0, m[0, 6], 1);

        Assert.Equal(0, m[1, 1], 1);
        Assert.Equal(0, m[1, 5], 1);

        Assert.Equal(1, m[2, 2], 1);
        Assert.Equal(3, m[2, 4], 1);

        Assert.Equal(5, m[3, 3], 1);

        Assert.Equal(7, m[4, 2], 1);
        Assert.Equal(9, m[4, 4], 1);

        Assert.Equal(0, m[5, 1], 1);
        Assert.Equal(0, m[5, 5], 1);

        Assert.Equal(0, m[6, 0], 1);
        Assert.Equal(0, m[6, 6], 1);
    }

    [Fact]
    public void ConvolveTest()
    {
        var matrix = TestMatrices.GetValueOrDefault(4);
        var kernel = Matrix<double>.Build.DenseOfRowArrays(new double[][]
        {
            new double[] {1.0, 0.5},
            new double[] {0.5, 1.0},
        });
        var result = CrossCorrelate(matrix, kernel);
        logger.WriteLine($"Crosscorrelation with matrix, kernel and result:\n{matrix}{kernel}{result}");
        Assert.Equal(10.5, result[0, 0], 1);    // 1 + 1 + 2.5 + 6
        Assert.Equal(13.5, result[0, 1], 1);    // 2 + 1.5 + 3 + 7
        Assert.Equal(16.5, result[0, 2], 1);    // 3 + 2 + 3.5 + 8

        Assert.Equal(22.5, result[1, 0], 1);    // 5 + 3 + 4.5 + 10
        Assert.Equal(25.5, result[1, 1], 1);    // 6 + 3.5 + 5 + 11
        Assert.Equal(28.5, result[1, 2], 1);    // 7 + 4 + 5.5 + 12

        Assert.Equal(34.5, result[2, 0], 1);    // 9 + 5 + 6.5 + 14,
        Assert.Equal(37.5, result[2, 1], 1);    // 10 + 5.5 + 7 + 15
        Assert.Equal(40.5, result[2, 2], 1);    // 11 + 6 + 7.5 + 16
    }

    [Fact]
    public void ConvolutionTest()
    {
        var matrix = TestMatrices.GetValueOrDefault(2);
        var kernel = TestMatrices.GetValueOrDefault(2);
        var fc = Convolve(matrix, kernel);
        logger.WriteLine($"Full convolution with matrix, kernel (rotated) and result:\n{matrix}{kernel.Rotate180()}{fc}");
        Assert.Equal(1, fc[0, 0], 1);
        Assert.Equal(4, fc[0, 1], 1);
        Assert.Equal(4, fc[0, 2], 1);
        Assert.Equal(6, fc[1, 0], 1);
        Assert.Equal(20, fc[1, 1], 1);
        Assert.Equal(16, fc[1, 2], 1);
        Assert.Equal(9, fc[2, 0], 1);
        Assert.Equal(24, fc[2, 1], 1);
        Assert.Equal(16, fc[2, 2], 1);
    }

    [Fact]
    public void MaxPoolingTest()
    {
        // 3x3 matrix, pool of 2x2
        var tm = TestMatrices.GetValueOrDefault(3);
        var mpd = MaxPooling(tm, 2);
        var mp = mpd.MaxPool;
        Assert.Equal(5, mp[0, 0]);
        Assert.Equal(1, mpd.FeatureMapMaxIndices[0].GetRow());
        Assert.Equal(1, mpd.FeatureMapMaxIndices[0].GetColumn());

        Assert.Equal(6, mp[0, 1]);
        Assert.Equal(1, mpd.FeatureMapMaxIndices[1].GetRow());
        Assert.Equal(2, mpd.FeatureMapMaxIndices[1].GetColumn());

        Assert.Equal(8, mp[1, 0]);
        Assert.Equal(2, mpd.FeatureMapMaxIndices[2].GetRow());
        Assert.Equal(1, mpd.FeatureMapMaxIndices[2].GetColumn());

        Assert.Equal(9, mp[1, 1]);
        Assert.Equal(2, mpd.FeatureMapMaxIndices[3].GetRow());
        Assert.Equal(2, mpd.FeatureMapMaxIndices[3].GetColumn());

        // 4x4 matrix, pool of 2x2
        tm = TestMatrices.GetValueOrDefault(4);
        mpd = MaxPooling(tm, 2);
        mp = mpd.MaxPool;

        Assert.Equal(6, mp[0, 0], 1);
        Assert.Equal(1, mpd.FeatureMapMaxIndices[0].GetRow());
        Assert.Equal(1, mpd.FeatureMapMaxIndices[0].GetColumn());

        Assert.Equal(8, mp[0, 1], 1);
        Assert.Equal(1, mpd.FeatureMapMaxIndices[1].GetRow());
        Assert.Equal(3, mpd.FeatureMapMaxIndices[1].GetColumn());

        Assert.Equal(14, mp[1, 0], 1);
        Assert.Equal(3, mpd.FeatureMapMaxIndices[2].GetRow());
        Assert.Equal(1, mpd.FeatureMapMaxIndices[2].GetColumn());

        Assert.Equal(16, mp[1, 1], 1);
        Assert.Equal(3, mpd.FeatureMapMaxIndices[3].GetRow());
        Assert.Equal(3, mpd.FeatureMapMaxIndices[3].GetColumn());

        // 4x4 matrix, pool of 3x3
        tm = TestMatrices.GetValueOrDefault(4);
        mpd = MaxPooling(tm, 3);
        mp = mpd.MaxPool;
        Assert.Equal(11, mp[0, 0], 1);
        Assert.Equal(2, mpd.FeatureMapMaxIndices[0].GetRow());
        Assert.Equal(2, mpd.FeatureMapMaxIndices[0].GetColumn());

        Assert.Equal(12, mp[0, 1], 1);
        Assert.Equal(2, mpd.FeatureMapMaxIndices[1].GetRow());
        Assert.Equal(3, mpd.FeatureMapMaxIndices[1].GetColumn());

        Assert.Equal(15, mp[1, 0], 1);
        Assert.Equal(3, mpd.FeatureMapMaxIndices[2].GetRow());
        Assert.Equal(2, mpd.FeatureMapMaxIndices[2].GetColumn());

        Assert.Equal(16, mp[1, 1], 1);
        Assert.Equal(3, mpd.FeatureMapMaxIndices[3].GetRow());
        Assert.Equal(3, mpd.FeatureMapMaxIndices[3].GetColumn());

        // 5x5 matrix, pool of 3x3
        tm = TestMatrices.GetValueOrDefault(5);
        mpd = MaxPooling(tm, 3);
        mp = mpd.MaxPool;
        Assert.Equal(13, mp[0, 0], 1);
        Assert.Equal(2, mpd.FeatureMapMaxIndices[0].GetRow());
        Assert.Equal(2, mpd.FeatureMapMaxIndices[0].GetColumn());

        Assert.Equal(15, mp[0, 1], 1);
        Assert.Equal(2, mpd.FeatureMapMaxIndices[1].GetRow());
        Assert.Equal(4, mpd.FeatureMapMaxIndices[1].GetColumn());

        Assert.Equal(23, mp[1, 0], 1);
        Assert.Equal(4, mpd.FeatureMapMaxIndices[2].GetRow());
        Assert.Equal(2, mpd.FeatureMapMaxIndices[2].GetColumn());

        Assert.Equal(25, mp[1, 1], 1);
        Assert.Equal(4, mpd.FeatureMapMaxIndices[3].GetRow());
        Assert.Equal(4, mpd.FeatureMapMaxIndices[3].GetColumn());
    }
}