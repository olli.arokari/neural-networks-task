﻿using NeuralNetworkAPI.Models;

namespace NeuralNetworkAPI.Logging
{
    public static class Log
    {
        private static LoggingLevel _loggingLevel = LoggingLevel.INFO;

        private static readonly string LogFile = "neural.log";

        public static LoggingLevel LoggingLevel
        {
            get { return _loggingLevel; }
            set { _loggingLevel = value; }
        }

        public static void Error(string message)
        {
            PrintMessage(LoggingLevel.ERROR, message);
        }

        public static void Info(string message)
        {
            if (LoggingLevel == LoggingLevel.INFO || LoggingLevel == LoggingLevel.DEBUG)
            {
                PrintMessage(LoggingLevel.INFO, message);
            }
        }

        public static void Debug(string message)
        {
            if (LoggingLevel.DEBUG == _loggingLevel)
            {
                PrintMessage(LoggingLevel.DEBUG, message);
            }
        }

        private static void PrintMessage(LoggingLevel logLevel, string message)
        {
            var time = DateTime.Now;
            string hour = time.ToString("HH");
            string min = time.ToString("mm");
            string sec = time.ToString("ss");
            Console.Write($"[{hour}:{min}:{sec}][");
            switch (logLevel)
            {
                case LoggingLevel.ERROR:
                    Console.ForegroundColor = ConsoleColor.Red;
                    break;

                case LoggingLevel.INFO:
                    Console.ForegroundColor = ConsoleColor.Cyan;
                    break;

                case LoggingLevel.DEBUG:
                    Console.ForegroundColor = ConsoleColor.Green;
                    break;
            }
            Console.Write($"{logLevel}");
            Console.ForegroundColor = ConsoleColor.White;
            if (logLevel == LoggingLevel.INFO) { Console.Write(" "); }
            Console.WriteLine($"]: {message}");

            WriteToLogFile($"[{hour}:{min}:{sec}][{logLevel}]: {message}");
        }

        private static void WriteToLogFile(string message)
        {
            FileInfo logfile = new(LogFile);
            var writer = logfile.AppendText();
            writer.WriteLine(message);
            writer.Close();
            writer.Dispose();
        }
    }
}