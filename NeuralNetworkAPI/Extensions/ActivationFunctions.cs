﻿using MathNet.Numerics.LinearAlgebra;
using MathNet.Numerics.LinearAlgebra.Double;
using NeuralNetworkAPI.Models;
using static NeuralNetworkAPI.Extensions.Utils;

namespace NeuralNetworkAPI.Extensions;

public static class ActivationFunctions
{
    public static Vector<double> Activate(this Vector<double> vector, ActivationFunction activationFunction)
    {
        var values = DenseVector.OfVector(vector);  // Is a deepcopy really necessary? rather, check each function separately if its necessary
        return activationFunction switch
        {
            ActivationFunction.Sigmoid => Sigmoid(values),
            ActivationFunction.ReLU => ReLU(values),
            ActivationFunction.LeakyReLU => LeakyReLU(values),
            ActivationFunction.Softmax => Softmax(values),
            _ => throw new Exception("Activation function " + activationFunction + " is not implemented"),
        };
    }

    public static Matrix<double> Activate(this Matrix<double> featureMap, ActivationFunction activationFunction)
    {
        var matrix = DenseMatrix.OfMatrix(featureMap);
        return activationFunction switch
        {
            ActivationFunction.Sigmoid => Sigmoid(matrix),
            ActivationFunction.ReLU => ReLU(matrix),
            ActivationFunction.LeakyReLU => LeakyReLU(matrix),
            _ => throw new NotImplementedException("Activation function " + activationFunction + " is not implemented"),
        };
    }

    public static Vector<double> DerivativeOf(this Vector<double> activatedValues, ActivationFunction activationFunction)
    {
        return activationFunction switch
        {
            ActivationFunction.Sigmoid => SigmoidDerivative(activatedValues),
            ActivationFunction.ReLU => ReLUDerivative(activatedValues),
            ActivationFunction.LeakyReLU => LeakyReLUDerivative(activatedValues),
            _ => throw new Exception("Activation function " + activationFunction + " is not implemented"),
        };
    }

    public static Matrix<double> DerivativeOf(this Matrix<double> m, ActivationFunction activationFunction)
    {
        return activationFunction switch
        {
            ActivationFunction.Sigmoid => SigmoidDerivative(m),
            ActivationFunction.ReLU => ReLUDerivative(m),
            ActivationFunction.LeakyReLU => LeakyReLUDerivative(m),
            _ => throw new Exception("Activation function " + activationFunction + " is not implemented"),
        };
    }

    private static Vector<double> ReLU(Vector<double> values)
    {
        var activatedValues = V.Dense(values.Count);
        for (int i = 0; i < values.Count; i++)
        {
            activatedValues[i] = values[i] <= 0 ? 0 : values[i];
        }
        return activatedValues;
    }

    private static Matrix<double> ReLU(Matrix<double> values)
    {
        var activatedValues = Matrix<double>.Build.Dense(values.RowCount, values.ColumnCount);
        for (int row = 0; row < values.RowCount; row++)
        {
            for (int col = 0; col < values.ColumnCount; col++)
            {
                activatedValues[row, col] = values[row, col] <= 0 ? 0 : values[row, col];
            }
        }
        return activatedValues;
    }

    private static Vector<double> ReLUDerivative(Vector<double> vector)
    {
        var rd = V.Dense(vector.Count);
        for (int i = 0; i < vector.Count; i++)
        {
            rd[i] = vector[i] <= 0 ? 0 : 1;
        }
        return rd;
    }

    private static Matrix<double> ReLUDerivative(Matrix<double> matrix)
    {
        var lrd = M.DenseOfMatrix(matrix);
        for (int i = 0; i < matrix.ColumnCount; i++)
        {
            var col = matrix.Column(i);
            var der = ReLUDerivative(col);
            lrd.SetColumn(i, der);
        }
        return lrd;
    }

    public static Vector<double> LeakyReLU(Vector<double> values)
    {
        var lr = V.Dense(values.Count);
        for (int i = 0; i < values.Count; i++)
        {
            lr[i] = values[i] * (values[i] <= 0 ? 0.1 : 1);
        }
        return lr;
    }

    private static Matrix<double> LeakyReLU(Matrix<double> values)
    {
        var lr = Matrix<double>.Build.Dense(values.RowCount, values.ColumnCount);
        for (int row = 0; row < values.RowCount; row++)
        {
            for (int col = 0; col < values.ColumnCount; col++)
            {
                lr[row, col] = values[row, col] * (values[row, col] <= 0 ? 0.1 : 1);
            }
        }
        return lr;
    }

    private static Vector<double> LeakyReLUDerivative(Vector<double> vector)
    {
        var lrd = V.Dense(vector.Count);
        for (int i = 0; i < vector.Count; i++)
        {
            lrd[i] = vector[i] <= 0 ? 0.1 : 1;
        }
        return lrd;
    }

    private static Matrix<double> LeakyReLUDerivative(Matrix<double> matrix)
    {
        var lrd = M.DenseOfMatrix(matrix);
        for (int i = 0; i < matrix.ColumnCount; i++)
        {
            var col = matrix.Column(i);
            var der = LeakyReLUDerivative(col);
            lrd.SetColumn(i, der);
        }
        return lrd;
    }

    private static Vector<double> Sigmoid(Vector<double> vector)
    {
        var sv = V.Dense(vector.Count);
        for (int i = 0; i < sv.Count; i++)
        {
            sv[i] = 1.0 / (1.0 + Math.Exp(-vector[i]));
        }
        return sv;
    }

    private static Matrix<double> Sigmoid(Matrix<double> matrix)
    {
        var result = M.DenseOfMatrix(matrix).Flatten(); // Copy and convert to array
        for (int i = 0; i < result.Length; i++)
        {   // run each value through the sigmoid function
            result[i] = 1.0 / (1.0 + Math.Exp(-result[i]));
        }
        return result.Unflatten();  // Convert back to matrix and return
    }

    private static Vector<double> SigmoidDerivative(Vector<double> vector)
    {
        // Assumes already activated values
        var dsv = V.Dense(vector.Count);
        for (int i = 0; i < vector.Count; i++)
        {
            dsv[i] = vector[i] * (1 - vector[i]);
        }
        return dsv;
    }

    private static Matrix<double> SigmoidDerivative(Matrix<double> matrix)
    {
        var sd = M.DenseOfMatrix(matrix).Flatten(); // Copy and convert to array
        for (int i = 0; i < sd.Length; i++)
        {   // run each value through the sigmoid derivative function
            sd[i] = sd[i] * (1 - sd[i]);
        }
        return sd.Unflatten();  // Convert back to matrix and return
    }

    private static Vector<double> Softmax(Vector<double> vector)
    {
        var softmax = DenseVector.OfVector(vector);
        // Denominator
        double sum = 0.0;
        for (int i = 0; i < vector.Count; i++)
        {
            softmax[i] = Math.Exp(vector[i]);
            sum += softmax[i];
        }
        // Numerator/result
        for (int i = 0; i < vector.Count; i++)
        {
            softmax[i] = softmax[i] / sum;
        }

        foreach (var s in softmax)
        {
            if (double.IsNaN(s))
            {
                throw new Exception("Softmax input was too large and produced NaN values");
            }
        }

        return softmax;
    }
}