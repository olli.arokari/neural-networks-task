﻿using MathNet.Numerics.LinearAlgebra;
using NeuralNetworkAPI.Models;
using static NeuralNetworkAPI.Extensions.Utils;

namespace NeuralNetworkAPI.Extensions
{
    public static class Equations
    {
        // Bias gradients of output layer
        public static Vector<double> Equation_1(Vector<double> output, int expectedNumber)
        {
            Vector<double> expectedOutput = V.Dense(10, 0);
            expectedOutput[expectedNumber] = 1; // onehot encoded
            return output - expectedOutput;
        }

        // Bias gradients of hidden layer
        public static Vector<double> Equation_2(Vector<double> outputError, Vector<double> rawOutput)
        {
            var d = ActivationFunctions.DerivativeOf(rawOutput, ActivationFunction.LeakyReLU);
            return outputError.PointwiseMultiply(d);
        }

        // Weight gradients
        public static Matrix<double> Equation_3(Vector<double> biasGradients, Vector<double> inputs)
        {
            // Change both from vectors to matrices and transpose the inputs
            return M.DenseOfColumnVectors(biasGradients) * M.DenseOfRowVectors(inputs);
        }

        // Input gradients or loss of previous layer
        public static Vector<double> Equation_4(Matrix<double> weights, Vector<double> biasGradients)
        {
            return weights.Transpose() * biasGradients;
        }
    }
}