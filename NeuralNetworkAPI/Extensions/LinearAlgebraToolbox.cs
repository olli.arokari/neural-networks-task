﻿using MathNet.Numerics.LinearAlgebra;
using static NeuralNetworkAPI.Extensions.Utils;

namespace NeuralNetworkAPI.Extensions
{
    public static class LinearAlgebraToolbox
    {
        /// <summary>
        /// Rotates the matrix 180
        /// </summary>
        /// <param name="matrix"></param>
        /// <returns>Rotated matrix</returns>
        public static Matrix<double> Rotate180(this Matrix<double> matrix)
        {
            var rotated = M.Dense(matrix.RowCount, matrix.ColumnCount);
            int dim = matrix.RowCount;  // assumes equal dimensions
            for (int row = 0; row < matrix.RowCount; row++)
            {
                for (int col = 0; col < matrix.ColumnCount; col++)
                {
                    rotated[row, col] = matrix[(dim - 1) - row, (dim - 1) - col];
                }
            }
            return rotated;
        }

        public static double[] Flatten(this Matrix<double> matrix)
        {
            List<double> list = new();
            for (int i = 0; i < matrix.RowCount; i++)
            {
                for (int j = 0; j < matrix.ColumnCount; j++)
                {
                    list.Add(matrix[i, j]);
                }
            }
            return list.ToArray();
        }

        public static double[] Flatten(this List<Matrix<double>> matrices)
        {
            List<double> flat = new();
            foreach (var matrix in matrices)
            {
                foreach (var val in matrix.Flatten())
                {
                    flat.Add(val);
                }
            }
            return flat.ToArray();
        }

        ///<summary>
        /// Converts an array of values into an equally dimensioned matrix
        /// </summary>
        /// <returns>The list of values as a matrix</returns>
        /// <exception cref="ArgumentException"></exception>
        public static Matrix<double> Unflatten(this double[] values)
        {
            int dimensions;
            try { dimensions = (int)Math.Sqrt(values.Length); }
            catch { throw new ArgumentException($"It is not possible to create an equally dimensioned matrix from {values.Length} values"); }

            Matrix<double> matrix = M.Dense(dimensions, dimensions);
            for (int row = 0; row < dimensions; row++)
            {
                for (int col = 0; col < dimensions; col++)
                {
                    matrix[row, col] = values[(row * dimensions) + col];
                }
            }
            return matrix;
        }

        public static Matrix<double> Unflatten(this Vector<double> vector)
        {
            return Unflatten(vector.ToArray<double>());
        }

        public static List<Matrix<double>> Unflatten(this Vector<double> vector, int matrixCount)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Adds layers of zeroes around the original matrix
        /// </summary>
        /// <param name="matrix"></param>
        /// <param name="padding"></param>
        public static Matrix<double> WithZeroPadding(this Matrix<double> matrix, int padding)
        {
            for (var i = 0; i < padding; i++)
            {
                matrix = matrix.InsertRow(
                    0,                          // row index
                    V.Dense(
                        matrix.ColumnCount,     // number of values in the inserted row
                        0));                    // initialize with zeroes
                matrix = matrix.InsertColumn(0, V.Dense(matrix.RowCount, 0));
                matrix = matrix.InsertRow(matrix.RowCount, V.Dense(matrix.ColumnCount, 0));
                matrix = matrix.InsertColumn(matrix.ColumnCount, V.Dense(matrix.RowCount, 0));
            }
            return matrix;
        }

        public static Matrix<double> RemoveZeroPadding(this Matrix<double> matrix)
        {
            // Figure out which rows/cols have only zero values
            List<int> rowsToRemove = new();
            List<int> colsToRemove = new();
            for (int row = 0; row < matrix.RowCount; row++) // TODO can be optimized if matrix is square
            {
                bool allZero = true;
                foreach (var d in matrix.Row(row))
                {
                    if (d > 0) { allZero = false; break; }
                }
                if (allZero) { rowsToRemove.Add(row); }
            }
            for (int col = 0; col < matrix.ColumnCount; col++)
            {
                bool allZero = true;
                foreach (var d in matrix.Column(col))
                {
                    if (d > 0) { allZero = false; break; }
                }
                if (allZero) { colsToRemove.Add(col); }
            }

            // Remove the rows/cols with only zero values (make a copy of the matrix not to edit the input matrix)
            var wop = M.DenseOfMatrix(matrix);  // deepcopy
            for (int i = rowsToRemove.Count - 1; i >= 0; i--)   // start from the right
            {
                int rowToRemove = rowsToRemove[i];
                wop = wop.RemoveRow(rowToRemove);
            }
            for (int i = colsToRemove.Count - 1; i >= 0; i--)
            {
                int colToRemove = colsToRemove[i];
                wop = wop.RemoveColumn(colToRemove);
            }

            // Make image square again
            if (wop.RowCount > wop.ColumnCount)
            {   // More rows than columns, add columns until the image is square again
                Vector<double> empty = V.Dense(wop.RowCount, 0);
                bool before = true; // Add the next column on the left side
                while (wop.RowCount != wop.ColumnCount)
                {
                    if (before) { wop = wop.InsertColumn(0, empty); }
                    else { wop = wop.InsertColumn(wop.ColumnCount, empty); }
                    before = !before;   // add every other on the left/right side
                }
            }
            else if (wop.ColumnCount > wop.RowCount)
            {   // More columns than rows, add rows until the image is square again
                Vector<double> empty = V.Dense(wop.ColumnCount, 0);
                bool before = true; // Add the next row on top
                while (wop.RowCount != wop.ColumnCount)
                {
                    if (before) { wop = wop.InsertRow(0, empty); }
                    else { wop = wop.InsertRow(wop.RowCount, empty); }
                    before = !before;   // add every other on the top/bottom
                }
            }
            return wop;
        }

        /// <returns>The sum of the values from the result of a pointwise multiplication of the two matrices</returns>
        public static double DotProduct(this Matrix<double> matrix1, Matrix<double> matrix2)
        {
            var sum = 0.0;
            for (int row = 0; row < matrix1.RowCount; row++)
            {
                for (int col = 0; col < matrix1.ColumnCount; col++)
                {
                    sum += (matrix1[row, col] * matrix2[row, col]);
                }
            }
            return sum;
        }

        public static void Cap(this Matrix<double> matrix, double cap)
        {
            for (int row = 0; row < matrix.RowCount; row++)
            {
                for (int col = 0; col < matrix.ColumnCount; col++)
                {
                    if (Math.Abs(matrix[row, col]) > cap) { matrix[row, col] = matrix[row, col] < 0 ? -cap : cap; }
                }
            }
        }

        public static void Cap(this List<Matrix<double>> listOfMatrices, double cap)
        {
            foreach (var matrix in listOfMatrices)
            {
                matrix.Cap(cap);
            }
        }
    }
}