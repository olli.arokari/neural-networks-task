﻿using MathNet.Numerics.LinearAlgebra;
using NeuralNetworkAPI.Models;

using static NeuralNetworkAPI.Extensions.Utils;

namespace NeuralNetworkAPI.Extensions;

public static class ConvolutionExtensions
{
    /// <param name="featureMap"></param>
    /// <param name="poolDimensions"></param>
    /// <returns>The max pool matrix, as well as the indexes of the location of the max values in the provided featuremap</returns>
    public static MaxPoolData MaxPooling(Matrix<double> featureMap, int poolDimensions)
    {
        var maxPoolData = new MaxPoolData();
        List<double> maxpool_1D = new();

        int dim = featureMap.RowCount;
        for (int row = 0; row < dim; row += poolDimensions)
        {
            for (int col = 0; col < dim; col += poolDimensions)
            {
                var rows = poolDimensions;   // How many rows to include
                var cols = poolDimensions;   // How many columns to include

                // If the pools don't match exactly the featuremap
                if (dim % poolDimensions != 0)
                {
                    // If the index of the col/row is for the last pool in this col/row
                    if (row >= dim - poolDimensions) { rows = dim % poolDimensions; }
                    if (col >= dim - poolDimensions) { cols = dim % poolDimensions; }
                }

                // Get the maximum value and its index
                double max = double.MinValue;
                int maxRowIndex = -1;
                int maxColIndex = -1;
                for (int subRow = 0; subRow < rows; subRow++)
                {
                    for (int subCol = 0; subCol < cols; subCol++)
                    {
                        var fmRowIndex = row + subRow;
                        var fmColIndex = col + subCol;
                        var value = featureMap[fmRowIndex, fmColIndex];
                        if (value > max)
                        {
                            max = value;
                            maxRowIndex = fmRowIndex;
                            maxColIndex = fmColIndex;
                        }
                    }
                }

                // Save
                maxpool_1D.Add(max);
                maxPoolData.FeatureMapMaxIndices.Add(new MatrixIndex(maxRowIndex, maxColIndex));
            }
        }
        maxPoolData.MaxPool = maxpool_1D.ToArray().Unflatten();
        return maxPoolData;
    }

    /// <summary>
    /// Opposite of pooling. Separates a list of values to match which kernel produced them.
    /// Used for separating the errors of the inputs to the fully connected neural network layers
    /// </summary>
    /// <param name="values"></param>
    /// <param name="kernelCount"></param>
    /// <returns></returns>
    public static List<double[]> Unpool(Vector<double> values, int kernelCount)
    {
        int valuesPerKernel = values.Count / kernelCount;   // values per kernel
        List<double[]> separatedValues = new();
        for (int i = 0; i < kernelCount; i++)
        {
            separatedValues.Add(new double[valuesPerKernel]);
            for (int j = 0; j < valuesPerKernel; j++)
            {
                separatedValues[i][j] = values[i * valuesPerKernel + j];
            }
        }
        return separatedValues;
    }

    /// <summary>
    /// Cross-correlate fully and with a 180 rotated kernel
    /// </summary>
    /// <param name="matrix"></param>
    /// <param name="kernel"></param>
    /// <returns></returns>
    public static Matrix<double> Convolve(Matrix<double> matrix, Matrix<double> kernel)
    {
        var m = matrix.WithZeroPadding(kernel.RowCount - 1);
        var rotated = kernel.Rotate180();
        return CrossCorrelate(m, rotated);
    }

    public static Matrix<double> CrossCorrelate(Matrix<double> matrix1, Matrix<double> matrix2)
    {
        var m1Dim = matrix1.RowCount;  // assumes a square shape
        var m2Dim = matrix2.RowCount; // also assumes a square shape
        var featureMap = M.Dense(m1Dim - (m2Dim - 1), m1Dim - (m2Dim - 1));
        for (int row = 0; row < featureMap.RowCount; row++)
        {
            for (int column = 0; column < featureMap.ColumnCount; column++)
            {
                var imageSample = matrix1.SubMatrix(row, m2Dim, column, m2Dim);
                double sum = imageSample.DotProduct(matrix2);
                featureMap[row, column] = sum;
            }
        }
        return featureMap;
    }

    public static List<Matrix<double>> ConvertToConvolvedOutput(Vector<double> vector, int matrices)
    {
        List<Matrix<double>> result = new();
        for (int i = 0; i < matrices; i++)
        {
            int valuesPerMatrix = vector.Count / matrices;
            var subVector = vector.SubVector(i * valuesPerMatrix, valuesPerMatrix);
            result.Add(subVector.Unflatten());
        }
        return result;
    }
}