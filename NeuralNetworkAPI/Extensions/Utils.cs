﻿using MathNet.Numerics.Distributions;
using MathNet.Numerics.LinearAlgebra;
using ScottPlot;
using ScottPlot.Drawing;
using ScottPlot.Renderable;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;

namespace NeuralNetworkAPI.Extensions;

public static class Utils
{
    public static bool GridsearchMode = false;
    public static readonly MatrixBuilder<double> M = Matrix<double>.Build;
    public static readonly VectorBuilder<double> V = Vector<double>.Build;
    public static readonly Random GlobalRandom = new(Seed: 7);

    /// <summary>
    /// Creates a string from an array of doubles.
    /// This assumes the values are pixel values of an image with equal dimensions.
    /// The output has five shades depending on the value.
    /// </summary>
    /// <param name="pixels"></param>
    /// <returns>A string representation of the pixels as an 2D array with 5 shades</returns>
    public static string ToPixelString(this double[] pixels, bool removeEmptyLines = false)
    {
        var min = pixels.Min();
        var max = pixels.Max();
        var diff = max - min;
        var dim = (int)Math.Sqrt(pixels.Length);
        var result = $"{dim}x{dim}, Low:{min}, High:{max}\n";
        for (int i = 0; i < pixels.Length; i++)
        {
            if (pixels[i] < min + (0.1 * diff)) { result += " "; }
            else if (pixels[i] < min + (0.25 * max)) { result += "░"; }
            else if (pixels[i] < min + (0.5 * max)) { result += "▒"; }
            else if (pixels[i] < min + (0.75 * max)) { result += "▓"; }
            else { result += "█"; }
            if (i % dim == dim - 1)
            {
                result += "\n";
            }
        }
        if (removeEmptyLines)
        {
            string emptyLine = "";
            for (int i = 0; i < dim; i++)
            {
                emptyLine += " ";
            }
            emptyLine += "\n";
            result = result.Replace(emptyLine, "");
        }
        return result;
    }

    public static string ToPixelString(this int[] pixels, bool removeEmptyLines = false)
    {
        double[] d = new double[pixels.Length];
        for (int i = 0; i < pixels.Length; i++)
        {
            d[i] = pixels[i];
        }
        return ToPixelString(pixels, removeEmptyLines);
    }

    /// <summary>
    /// Creates a string from a matrix of doubles.
    /// The output has five shades depending on the value.
    /// </summary>
    /// <param name="pixels"></param>
    /// <returns>A string representation of the pixels as an 2D array with 5 shades</returns>
    public static string ToPixelString(this Matrix<double> pixels)
    {
        return pixels.ToRowMajorArray().ToPixelString();
    }

    public static void PlotGraph(string xAxisLabel, string name, double[] y1, string name2 = "", double[]? y2 = null, string? title = "")
    {
        Dictionary<int, int> resolutions = new()
                {
                    //{ 320, 240 },
                    { 400, 300 },   // Excellent for the report
                    //{ 640, 480 },
                    //{ 800, 600 },
                    //{ 1200, 800 },
                    { 1600, 1200 }  // Zoomable
                };
        foreach (var res in resolutions)
        {
            var plt = new Plot(res.Key, res.Value);

            if (!"".Equals(title)) { plt.Title(title); }
            if (!"".Equals(xAxisLabel)) { plt.XAxis.Label(xAxisLabel); }

            // Plot the data
            var signal1 = plt.AddSignal(y1);
            signal1.YAxisIndex = 0;
            plt.YAxis.Label(name, signal1.Color);
            plt.YAxis.Color(signal1.Color);

            // Plot second graph if present
            if (!(y2 is null || "".Equals(y2)))
            {
                var signal2 = plt.AddSignal(y2);
                var rightAxis = plt.AddAxis(Edge.Right, axisIndex: 3, color: signal2.Color);
                signal2.YAxisIndex = 3;
                rightAxis.Label(name2, signal2.Color);
            }

            var path = Path.Combine("Results", $"{res.Value}p_{(!"".Equals(title) ? title + "_" : "")}{name.ToLower().Replace(" ", "_")}{(name2 != null ? "_" + name2.Replace(" ", "_") : "")}.png");
            var file = new FileInfo(path);
            if (file.Exists) { File.SetAttributes(path, ~FileAttributes.ReadOnly); } // removes the read only property so the file can be overwritten
            plt.SaveFig(path);
            File.SetAttributes(path, FileAttributes.ReadOnly);  // Bandaid for Windows Photos bug that prevents the user for viewing the next photo if the image is not read-only
        }
    }

    public static void PlotHeatmap(int epoch, string what, int index, double[,] data)
    {
        var plt = new Plot(400, 300);

        var hm = plt.AddHeatmap(data, colormap: Colormap.Grayscale);
        plt.AddColorbar(hm);

        string resultsDirPath = Path.Combine("Results", $"{what}_heatmaps");
        DirectoryInfo resultsDir = new(resultsDirPath);
        if (!resultsDir.Exists) { resultsDir.Create(); }

        plt.SaveFig(Path.Combine(resultsDirPath, $"{what}_{(index == -1 ? "00" : index)}_epoch_{epoch}.png"));
    }

    public static double GetInitialWeight(int lastLayerNeuronCount)
    {
        // "He" -initialization:
        var standardDeviation = Math.Sqrt(2.0 / lastLayerNeuronCount);
        var mean = 0.0;
        return Normal.Sample(mean, standardDeviation);
    }

    public static double GetInitialKernelWeight(int featuremapSize)
    {
        return GetInitialWeight(featuremapSize);
    }

    public static double GetRandomDouble(double start, double end)
    {
        return ((end - start) * GlobalRandom.NextDouble()) + start;
    }

    /// <summary>
    ///     Resize the image to the specified width and height.
    /// </summary>
    /// <remarks>
    ///     Shamelessly borrowed from stack overflow https://stackoverflow.com/questions/1922040/how-to-resize-an-image-c-sharp
    ///     Author: mpen
    /// </remarks>
    /// <param name="image">The image to resize.</param>
    /// <param name="width">The width to resize to.</param>
    /// <param name="height">The height to resize to.</param>
    /// <returns>The resized image.</returns>
    public static double[] ResizeImage(this double[] pixels, int width, int height)
    {
        // Create a bitmap of the pixels
        int dim = (int)Math.Round(Math.Sqrt(pixels.Length), 0);
        Bitmap image = new(dim, dim);
        for (int x = 0; x < dim; x++)   // x= column of bitmap
        {
            for (int y = 0; y < dim; y++)  // y= row of bitmap
            {
                int index = (dim * x) + (dim - 1) - y;
                int gs = (int)Math.Round(pixels[index] * 255.0, 0);   // gs= pixel grayscale value, 0-255 (the pixel array has values from 0 to 1, a translation back to 255 is required
                Color color = Color.FromArgb(gs, gs, gs);   // Color is required by Bitmap, but using the rule R=G=B=Grayscale gives us the same result.
                image.SetPixel(x, y, color);
            }
        }

        // From Stack overflow on how to resize the bitmap
        var destRect = new Rectangle(0, 0, width, height);
        //Log.Debug($"Trying to create a new bitmap with {width} and {height}");
        var destImage = new Bitmap(width, height);

        destImage.SetResolution(image.HorizontalResolution, image.VerticalResolution);

        using (var graphics = Graphics.FromImage(destImage))
        {
            graphics.CompositingMode = CompositingMode.SourceCopy;
            graphics.CompositingQuality = CompositingQuality.HighQuality;
            graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
            graphics.SmoothingMode = SmoothingMode.HighQuality;
            graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;

            using var wrapMode = new ImageAttributes();
            wrapMode.SetWrapMode(WrapMode.TileFlipXY);
            graphics.DrawImage(image, destRect, 0, 0, image.Width, image.Height, GraphicsUnit.Pixel, wrapMode);
        }

        // Convert back to an array of pixels and grayscale values between 0 and 1
        List<double> newPixels = new();
        for (int x = 0; x < destImage.Width; x++)
        {
            for (int y = destImage.Height - 1; y >= 0; y--)
            {
                newPixels.Add(destImage.GetPixel(x, y).R / 255.0);
            }
        }
        destImage.Dispose();
        return newPixels.ToArray();
    }
}