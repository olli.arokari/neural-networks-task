﻿using MathNet.Numerics.LinearAlgebra;
using NeuralNetworkAPI.Extensions;
using static NeuralNetworkAPI.Extensions.ActivationFunctions;
using static NeuralNetworkAPI.Extensions.ConvolutionExtensions;
using static NeuralNetworkAPI.Extensions.Utils;

namespace NeuralNetworkAPI.Models;

public class ConvolutionalLayer
{
    private List<Matrix<double>> Input { get; set; }
    private ActivationFunction ActivationFunction { get; set; }
    public int OutputDimensions { get; set; }
    public int OutputCount { get => Input.Count * Kernels.Count; }
    public int OutputPixels { get => (int)Math.Pow(OutputDimensions, 2) * OutputCount; }
    public int FeatureMapDim { get; set; }
    public int MaxPool_PoolDim { get; set; }    // Size of a single pool, not the resulting maxpooled image

    public List<Matrix<double>> Kernels { get; set; }
    public List<Matrix<double>> Biases { get; set; }
    public List<List<MatrixIndex>> MPFMI { get; set; }

    public ConvolutionalLayer(int inputs, int inputDim, int kernels, int kernelDim, ActivationFunction activationFunction, int poolSize = 1)
    {
        ActivationFunction = activationFunction;

        Input = new(); for (int i = 0; i < inputs; i++) { Input.Add(M.Dense(1, 1, 0)); }
        FeatureMapDim = inputDim - (kernelDim - 1);
        MaxPool_PoolDim = poolSize;
        OutputDimensions = (int)Math.Floor(FeatureMapDim * 1.0 / MaxPool_PoolDim);
        OutputDimensions += (FeatureMapDim % MaxPool_PoolDim) == 0 ? 0 : 1;

        Kernels = new();
        Biases = new();
        MPFMI = new();

        for (int i = 0; i < kernels; i++)
        {
            MPFMI.Add(new List<MatrixIndex>());
            Biases.Add(M.Dense(FeatureMapDim, FeatureMapDim, 0)); // Bias matrix has the same dimension as featuremap, initialized with zeros
            Kernels.Add(M.Dense(kernelDim, kernelDim, 0));
            for (int row = 0; row < kernelDim; row++)
            {
                for (int col = 0; col < kernelDim; col++)
                {
                    Kernels[i][row, col] = GetInitialKernelWeight(FeatureMapDim);
                }
            }
        }
    }

    public List<Matrix<double>> ForwardPropagate(List<Matrix<double>> input)
    {
        Input = input;
        List<Matrix<double>> outputs = new();
        foreach (var image in Input)
        {
            for (int i = 0; i < Kernels.Count; i++)
            {
                // Convolve, add bias and activate
                var featureMap = CrossCorrelate(image, Kernels[i]);
                featureMap += Biases[i];
                featureMap = ActivationFunctions.Activate(featureMap, ActivationFunction);

                // Max pooling
                if (MaxPool_PoolDim > 1) // Skip maxpooling when poolsize is 1 (= no pooling)
                {   // Pool, store the indices of the max values and add the result to the output
                    MaxPoolData mpd = MaxPooling(featureMap, MaxPool_PoolDim);

                    MPFMI[i] = mpd.FeatureMapMaxIndices;
                    outputs.Add(mpd.MaxPool);
                }
                else
                {
                    outputs.Add(featureMap);
                }
            }
        }
        return outputs;
    }

    public List<Matrix<double>> Backpropagate(List<Matrix<double>> error, double learningRate, bool isFirstLayer)
    {
        List<Matrix<double>> inputErrors = new();

        foreach (var input in Input)
        {
            Matrix<double> inputError = M.Dense(Input[0].RowCount, Input[0].ColumnCount, 0);
            for (int i = 0; i < Kernels.Count; i++)
            {
                Matrix<double> featureMapErr = error[i].DerivativeOf(ActivationFunction);
                if (MaxPool_PoolDim > 1)
                {   // Create a "featuremap" containing only the errors of the max values
                    var kernelErrors = featureMapErr.Flatten();
                    featureMapErr = M.Dense(FeatureMapDim, FeatureMapDim, 0); // Set correct dimensions and all values to zero
                    for (int j = 0; j < MPFMI[i].Count; j++)
                    {
                        var row = MPFMI[i][j].GetRow();
                        var col = MPFMI[i][j].GetColumn();
                        featureMapErr[row, col] = kernelErrors[j];
                    }
                }

                // Update bias
                Biases[i] = Biases[i] - (featureMapErr * learningRate);

                // Update kernel gradients (inputs cross-correlated with the featuremap errors)
                var kernelGradients = CrossCorrelate(input, featureMapErr);
                Kernels[i] = Kernels[i] - (kernelGradients * learningRate);

                // Partial derivative of loss with respect to one of the input images
                if (isFirstLayer) { continue; } // Skips calculating the error of the input when the input is the original image
                var ie = Convolve(featureMapErr, Kernels[i]);
                inputError += ie; // Sum the errors of this particular input
            }
            inputErrors.Add(inputError);
        }
        // return the error of the input to this layer, in other words the error of the output of the previous layer
        return inputErrors;
    }
}