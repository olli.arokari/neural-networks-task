﻿namespace NeuralNetworkAPI.Models;

public class MatrixIndex
{
    private readonly int row;

    private readonly int column;

    public MatrixIndex(int row, int column)
    {
        this.row = row;
        this.column = column;
    }

    public int GetRow()
    {
        return row;
    }

    public int GetColumn()
    {
        return column;
    }
}