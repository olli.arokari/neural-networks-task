﻿using System.ComponentModel.DataAnnotations;

namespace NeuralNetworkAPI.Models;

public class GrayScaleImage
{
    [Required]
    public int[]? Data { get; set; }

    public int Height { get; set; }

    public int Width { get; set; }
}