﻿using MathNet.Numerics.LinearAlgebra;
using NeuralNetworkAPI.Data;
using NeuralNetworkAPI.Extensions;
using NeuralNetworkAPI.Logging;
using static NeuralNetworkAPI.Extensions.ConvolutionExtensions;
using static NeuralNetworkAPI.Extensions.Utils;

namespace NeuralNetworkAPI.Models;

public class NeuralNetwork
{
    private readonly int ImageSize;
    private readonly double ConvolutionalLearningRate;
    private readonly double ClassifierLearningRate;
    private List<ConvolutionalLayer> ConvolutionalLayers { get; set; }
    private List<FullyConnectedLayer> ClassifierLayers { get; set; }

    public NeuralNetwork(int inputImageWidth, double classifierLearningRate = 0.01, double convolutionalLearningRate = 0.001)
    {
        ImageSize = inputImageWidth;
        ConvolutionalLearningRate = convolutionalLearningRate;
        ClassifierLearningRate = classifierLearningRate;
        ConvolutionalLayers = new();
        ClassifierLayers = new();
    }

    public void AddConvolutionalLayer(int kernels, int kernelSize, ActivationFunction activationFunction, int poolSize = 1)
    {
        if (ClassifierLayers.Count == 0)
        {
            int inputs = 1;
            int inputDim = ImageSize;
            if (ConvolutionalLayers.Count > 0)
            {
                var lastLayer = ConvolutionalLayers[^1];
                inputDim = lastLayer.OutputDimensions;
                inputs = lastLayer.OutputCount;
            }
            ConvolutionalLayer newLayer = new(inputs, inputDim, kernels, kernelSize, activationFunction, poolSize);
            ConvolutionalLayers.Add(newLayer);
            //Log.Debug($"Added convolutional layer: {inputDim}x{inputDim}x{inputs} -> {newLayer.OutputDimensions}x{newLayer.OutputDimensions}x{newLayer.OutputCount}");
        }
        else { throw new InvalidOperationException("Cannot add convolutional layers after classifier layers"); }
    }

    public void AddClassifierLayer(int neurons, ActivationFunction activationFunction)
    {
        var inputSize = (int)Math.Pow(ImageSize, 2);
        if (ClassifierLayers.Count > 0)
        {
            // This layer will be after a classifier layer
            inputSize = ClassifierLayers[^1].Output.Count;
        }
        else if (ConvolutionalLayers.Count > 0)
        {
            // There are no existing classifier layers, this layer will be after a convolutional layer
            inputSize = ConvolutionalLayers[^1].OutputPixels; //(int)Math.Pow(ConvolutionalLayers[^1].OutputDimensions, 2);
        }
        // else -> this is the first layer, use image pixel count as input size
        ClassifierLayers.Add(new(inputSize, neurons, activationFunction));
        //Log.Debug($"Added classifier layer: {inputSize} -> {neurons}");
    }

    public int Guess(double[] pixels)
    {
        return Guess(V.DenseOfArray(pixels));
    }

    public int Guess(Vector<double> pixels)
    {
        // Forward propagate convolutional layers
        List<Matrix<double>> input = new() { pixels.Unflatten() };
        foreach (var cLayer in ConvolutionalLayers)
        {
            input = cLayer.ForwardPropagate(input);
        }

        // Forward propagete classifier layers
        Vector<double> classifierInput = V.DenseOfArray(input.Flatten());
        foreach (var layer in ClassifierLayers)
        {
            classifierInput = layer.Forwardpropagate(classifierInput);
        }

        // The guessed number is the last layers output's largest numbers index in that output
        return classifierInput.MaximumIndex(); ;
    }

    public void Train(Vector<double> pixels, int expectedNumber)
    {
        try
        {
            List<Matrix<double>> input = new() { pixels.Unflatten() };

            // Forward propagate (set the (raw/activated) output values which are used in backpropagation)
            foreach (var cLayer in ConvolutionalLayers)
            {
                input = cLayer.ForwardPropagate(input);
            }
            Vector<double> classifierInput = V.DenseOfArray(input.Flatten());

            foreach (var layer in ClassifierLayers)
            {
                classifierInput = layer.Forwardpropagate(classifierInput);
            }

            // Backpropagate (adjust weights and biases)
            var inputError = ClassifierLayers[^1].Backpropagate(ClassifierLearningRate, null, true, expectedNumber);
            for (int i = ClassifierLayers.Count - 2; i >= 0; i--)
            {
                inputError = ClassifierLayers[i].Backpropagate(ClassifierLearningRate, inputError);
            }
            if (ConvolutionalLayers.Count != 0)
            {
                var cInputError = ConvertToConvolvedOutput(inputError, ConvolutionalLayers[^1].OutputCount);
                for (int i = ConvolutionalLayers.Count - 1; i >= 0; i--)
                {
                    cInputError = ConvolutionalLayers[i].Backpropagate(cInputError, ConvolutionalLearningRate, i == 0);
                }
            }
        }
        catch (Exception exc)
        {
            Log.Error("Training the neural network failed: \n" + exc.Message);
            List<double> weights = new();
            List<double> biases = new();
            foreach (var layer in ConvolutionalLayers)
            {
                foreach (var d in layer.Kernels.Flatten()) { weights.Add(d); }
                foreach (var b in layer.Biases.Flatten()) { biases.Add(b); }
            }
            foreach (var layer in ClassifierLayers)
            {
                foreach (var d in layer.Weights.Flatten()) { weights.Add(d); }
                foreach (var b in layer.Biases) { biases.Add(b); }
            }
            weights.Sort();
            biases.Sort();
            string largestWeights = $"Largest weights: \n";
            for (int i = 0; i < 10; i++)
            {
                largestWeights += $"{weights[i]}\n";
            }
            string largestBiases = $"Largest biases: \n";
            for (int i = 0; i < 10; i++)
            {
                largestBiases += $"{biases[i]}\n";
            }
            Log.Debug(largestWeights);
            Log.Debug(largestBiases);

            throw exc;
        }
    }

    public double GetLastLoss(int expectedNumber)
    {
        return -Math.Log(ClassifierLayers[^1].Output[expectedNumber]);
    }

    public List<Matrix<double>> GetKernels()
    {
        List<Matrix<double>> kernels = new();
        foreach (var cLayer in ConvolutionalLayers)
        {
            foreach (var kernel in cLayer.Kernels)
            {
                kernels.Add(kernel);
            }
        }
        return kernels;
    }

    internal List<Matrix<double>> GetConvolutionalLayerOutput(HandwrittenNumber hwn)
    {
        List<Matrix<double>> convolvedImages = new();
        foreach (var cLayer in ConvolutionalLayers)
        {
            convolvedImages.AddRange(cLayer.ForwardPropagate(new List<Matrix<double>>() { hwn.Pixels.Unflatten() }));
        }
        return convolvedImages;
    }
}