﻿using MathNet.Numerics.LinearAlgebra;
using NeuralNetworkAPI.Extensions;
using static NeuralNetworkAPI.Extensions.Equations;
using static NeuralNetworkAPI.Extensions.Utils;

namespace NeuralNetworkAPI.Models;

public class FullyConnectedLayer
{
    private ActivationFunction ActivationFunction { get; set; }
    public Vector<double> Input { get; set; }
    public Vector<double> RawOutput { get; set; }
    public Vector<double> Output { get; set; }

    public Vector<double> Biases { get; set; }
    public Vector<double> BiasGradients { get; set; }
    public Matrix<double> Weights { get; set; }
    public Matrix<double> WeightGradients { get; set; }

    public FullyConnectedLayer(int inputSize, int outputSize, ActivationFunction activationFunction)
    {
        Input = V.Dense(inputSize);
        RawOutput = V.Dense(outputSize);
        Output = V.Dense(outputSize);
        Biases = V.Dense(outputSize, 0);
        BiasGradients = V.Dense(outputSize);
        Weights = M.Dense(outputSize, inputSize);
        WeightGradients = M.Dense(outputSize, inputSize);
        ActivationFunction = activationFunction;

        // Set random weights (biases start at zero)
        for (int row = 0; row < Weights.RowCount; row++)
        {
            for (int col = 0; col < Weights.ColumnCount; col++)
            {
                Weights[row, col] = GetInitialWeight(inputSize);
            }
        }
    }

    /// <summary>
    /// Set the raw and activated output values of this layer according to the input
    /// </summary>
    /// <param name="input"></param>
    /// <returns> The activated output = the input of the next layer</returns>
    public Vector<double> Forwardpropagate(Vector<double> input)
    {
        Input = input;                  // Save the input to this layer for use in backpropagation
        RawOutput = Weights * Input;    // The raw output is calculated by multiplying the weight matrix by the input (a vector)
        RawOutput += Biases;            // Add the bias to the raw output
        Output = ActivationFunctions.Activate(RawOutput, ActivationFunction);
        return Output;
    }

    /// <summary>
    /// Adjust the biases and weights according to the input, expected output and learning rate
    /// </summary>
    /// <param name="input"> Input to this layer when forward propagating. Equals the output of the layer before. </param>
    /// <param name="outputError"> Not required if this is the last layer </param>
    /// <param name="isOutputLayer"></param>
    /// <param name="expectedNumber"></param>
    /// <returns>The error of input to this layer = The error of the output of the layer before </returns>
    public Vector<double> Backpropagate(double learningRate, Vector<double>? outputError, bool isOutputLayer = false, int expectedNumber = -1)
    {
        // Bias gradients
        BiasGradients = isOutputLayer switch
        {
            true => Equation_1(Output, expectedNumber),
            false => Equation_2(outputError, RawOutput)
        };

        // Weight gradients
        WeightGradients = Equation_3(BiasGradients, Input);

        // Error of the input = error of the output of the layer before
        var inputError = Equation_4(Weights, BiasGradients);

        // Update the biases and weights
        Biases -= BiasGradients * learningRate;
        Weights -= WeightGradients * learningRate;

        // Partial derivative of loss with respect to input
        return inputError;
    }
}