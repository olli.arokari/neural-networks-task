﻿using MathNet.Numerics.LinearAlgebra;

namespace NeuralNetworkAPI.Models;

public class MaxPoolData
{
    public Matrix<double> MaxPool { get; set; }
    public List<MatrixIndex> FeatureMapMaxIndices { get; set; }

    public MaxPoolData()
    {
        MaxPool = Matrix<double>.Build.Dense(1, 1);
        FeatureMapMaxIndices = new();
    }
}