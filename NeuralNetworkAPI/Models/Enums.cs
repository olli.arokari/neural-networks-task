﻿namespace NeuralNetworkAPI.Models;

public enum ActivationFunction
{
    Sigmoid,
    ReLU,
    LeakyReLU,
    Softmax
}

public enum LoggingLevel
{
    ERROR,
    INFO,
    DEBUG
}